
# Scavenge

scavenge is a platform for online scavenger hunts.
If you want to develop scavange.io on your local machine there are usefull scripts in the `package.json`. Dependencies are `npm, node` and `docker`. `docker-compose` can be used optionally.

## Quickstart
To build and run scavenge:
```shell
$ docker-compose up install && docker-compose up build
$ docker-compose up db redis scavenge
```

The server will listen on port 8080.

## Development

Running scavenge in develop is not supported with docker, consider using the open source Arch Linux operating system instead ;)

### Install dependencies
#### With Docker
```shell
$ docker-compose up install
```
#### Without Docker
```shell
$ npm install && npm run install
```

### Run
To run backend and frontend in hot reloading mode, with redis and mariadb in docker
```shell
$ npm run develop
```

To run backend and frontend in hot reloading mode, without redis and mariadb in docker (usefull if you have already local instances of mariadb/redis running) ATTENTION: you need to create a database named `scavenge` in mariadb and check the configuration in the `backend/develop.config.json`
```shell
$ npm run dev
```

The server will listen on port 8000.

If you want to insert test data, you can set the environment variable `TEST_DATA=1`. For Linux / OS X run:
```shell
$ export TEST_DATA=1
```

Have fun!

## Production Build

To build scavenge for production you need to install all dependencies like in the local development section. Make sure to edit the `backend/production.config.json` in the toplevel folder of the repo to your needs, default is production run with docker.

### With Docker

```shell
$ docker-compose up build
```

### Without Docker 

```shell
$ npm run build
```

## Production Run

To run scavenge in prodcution you can just run the commands below or if you don't have a redis or mariadb database on your server you can also start those along with scavenge.

The server will listen on port 8080.

### Without Docker

In `backend/` run
```shell
$ npm run prod
```
Mariadb and redis hostnames may need to be adjusted in `backend/production.config.json`.

### With Docker including mariadb and redis

```shell
$ docker-compose up scavenge db redis
```

### With Docker without mariadb and redis

```shell
$ docker-compose up scavenge
```

## Documentation

Scavenge also got a really nice documentation, you can build the docs locally or with docker

### Without Docker

```shell
$ npm run doc
```

### With Docker

```shell
$ docker-compose up doc
```