import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { IonicModule } from '@ionic/angular';
import { CalendarModule } from 'ion2-calendar';
import { NgxGraphModule } from '@swimlane/ngx-graph';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeViewComponent } from './views/home-view/home-view.component';
import { AboutViewComponent } from './views/about-view/about-view.component';
import { HuntsViewComponent } from './views/hunts-view/hunts-view.component';
import { PageNotFoundViewComponent } from './views/page-not-found-view/page-not-found-view.component';
import { RiddleEditComponent } from './components/riddle-edit/riddle-edit.component';
import { HuntEditViewComponent } from './views/hunt-edit-view/hunt-edit-view.component';
import { HuntSolveViewComponent } from './views/hunt-solve-view/hunt-solve-view.component';
import { LoginViewComponent } from './views/login-view/login-view.component';
import { ProfileViewComponent } from './views/profile-view/profile-view.component';
import { RegisterViewComponent } from './views/register-view/register-view.component';
import { HuntCreateViewComponent } from './views/hunt-create-view/hunt-create-view.component';

import { LOCALE_ID } from '@angular/core';

import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import { ContactViewComponent } from './views/contact-view/contact-view.component';
import { HuntsTableEditEntryComponent } from './components/hunts-table-edit-entry/hunts-table-edit-entry.component';
import { HuntsTableSolveEntryComponent } from './components/hunts-table-solve-entry/hunts-table-solve-entry.component';
import { RiddleEditModalComponent } from './components/riddle-edit-modal/riddle-edit-modal.component';
import { RiddleEditListComponent } from './components/riddle-edit-list/riddle-edit-list.component';
import { ServerErrorViewComponent } from './views/server-error-view/server-error-view.component';
import { RiddleSolveComponent } from './components/riddle-solve/riddle-solve.component';
import { RiddleSolveListComponent } from './components/riddle-solve-list/riddle-solve-list.component';
import { RiddleViewComponent } from './components/riddle-view/riddle-view.component';
import { RiddleViewListComponent } from './components/riddle-view-list/riddle-view-list.component';
import { HuntEditModalComponent } from './components/hunt-edit-modal/hunt-edit-modal.component';
import { HuntGraphComponent } from './components/hunt-graph/hunt-graph.component';
import { SolveViewComponent } from './views/solve-view/solve-view.component';
import { TutorialViewComponent } from './views/tutorial-view/tutorial-view.component';
import { HintEditModalComponent } from './components/hint-edit-modal/hint-edit-modal.component';

registerLocaleData(localeDe, 'de');

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeViewComponent,
    AboutViewComponent,
    HuntsViewComponent,
    HuntEditModalComponent,
    PageNotFoundViewComponent,
    RiddleEditComponent,
    HuntEditViewComponent,
    HuntSolveViewComponent,
    LoginViewComponent,
    ProfileViewComponent,
    RegisterViewComponent,
    HuntCreateViewComponent,
    ContactViewComponent,
    HuntsTableEditEntryComponent,
    HuntsTableSolveEntryComponent,
    RiddleEditModalComponent,
    RiddleEditListComponent,
    ServerErrorViewComponent,
    RiddleSolveComponent,
    RiddleSolveListComponent,
    RiddleViewComponent,
    RiddleViewListComponent,
    HuntEditModalComponent,
    HuntGraphComponent,
    SolveViewComponent,
    TutorialViewComponent,
    HintEditModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    CalendarModule,
    NgxGraphModule,
    NgxChartsModule,
    BrowserAnimationsModule,
    NgbTooltipModule,
    IonicModule.forRoot()
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'de' }],
  bootstrap: [AppComponent],
  entryComponents: [
    RiddleEditModalComponent,
    HuntEditModalComponent,
    HintEditModalComponent
  ]
})
export class AppModule { }
