import { Component, OnInit, Input } from '@angular/core';
import { HuntDataService } from 'src/app/services/hunt-data.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Graph } from '../../../../../backend/src/endpoints/responses';

@Component({
  selector: 'app-hunt-graph',
  templateUrl: './hunt-graph.component.html',
  styleUrls: ['./hunt-graph.component.scss']
})
export class HuntGraphComponent implements OnInit {
  @Input() huntID: number;

  graphData: Graph = { nodes: [], links: [] };

  constructor(public dataService: HuntDataService) { }

  ngOnInit() {
    this.updateData();
  }

  updateData() {
    this.dataService.getHuntGraph(this.huntID).subscribe(
      res => {
        if (res instanceof HttpErrorResponse) {
          console.error(res);
        } else {
          this.graphData.nodes = res.nodes;
          this.graphData.links = res.links;

          // trigger window resize event after a few ms to circumvent bug in ngx
          setTimeout(() => {
            const resizeEvent = window.document.createEvent('UIEvents');
            resizeEvent.initUIEvent('resize', true, false, window, 0);
            window.dispatchEvent(resizeEvent);
          }, 10);
        }
      }
    );
  }
}
