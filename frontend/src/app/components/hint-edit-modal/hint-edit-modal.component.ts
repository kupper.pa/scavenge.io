import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-hint-edit-modal',
  templateUrl: './hint-edit-modal.component.html',
  styleUrls: ['./hint-edit-modal.component.scss']
})
export class HintEditModalComponent {

  hintEditForm = new FormGroup({
    title: new FormControl(),
    description: new FormControl(),
    displayInHours: new FormControl(),
  });

  constructor(public modalController: ModalController, public alertService: AlertService, params: NavParams) {
    this.hintEditForm.controls.title.setValue(params.get('title'));
    this.hintEditForm.controls.description.setValue(params.get('description'));
    this.hintEditForm.controls.displayInHours.setValue(params.get('displayInHours'));
  }

  /**
   * Executed when submit button is pressed. Checks for valid input and hands it to the calling component when successful
   */
  onHintEditFormSubmit() {
    // input validation
    const title = this.hintEditForm.controls.title.value;
    if (!title) {
      this.alertService.showWarningAlert('Hinweis bearbeiten', 'Bitte geben sie einen Titel an');
      return;
    }

    const description = this.hintEditForm.controls.description.value;
    if (!description) {
      this.alertService.showWarningAlert('Hinweis bearbeiten', 'Bitte geben sie eine Beschreibung an');
      return;
    }

    const displayInHours = this.hintEditForm.controls.displayInHours.value;
    if (!(typeof displayInHours === 'number') || displayInHours < 0) {
      this.alertService.showWarningAlert('Hinweis bearbeiten', 'Bitte geben sie eine gültige Anzeigezeit an');
      return;
    }

    // return input data to calling component
    this.modalController.dismiss({ title, description, displayInHours });
  }

  /**
   * Is called when riddle edit is cancelled
   */
  onHintEditFormDismissed() {
    // dont return any data
    this.modalController.dismiss();
  }

}
