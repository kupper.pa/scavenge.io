import { Component, Input, EventEmitter, Output } from '@angular/core';
import { File, Riddle } from '../../../../../backend/src/endpoints/responses';
import { HuntDataService } from 'src/app/services/hunt-data.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { FileService } from 'src/app/services/file.service';
import { HttpErrorResponse } from '@angular/common/http';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-riddle-solve',
  templateUrl: './riddle-solve.component.html',
  styleUrls: ['./riddle-solve.component.scss']
})
/**
 * Contains information and controls about a riddle which enable the user to solve it if they know the solution
 */
export class RiddleSolveComponent {
  @Input() riddle: Riddle;

  @Output() riddleSolved: EventEmitter<number> = new EventEmitter<number>();

  solveForm = new FormGroup({
    solution: new FormControl(),
  });

  progress: Observable<number>;

  constructor(public dataService: HuntDataService, public fileService: FileService, public alertService: AlertService) { }

  onSolveFormSubmit() {
    this.dataService.solveRiddle(this.riddle.id, { solution: this.solveForm.controls.solution.value }).subscribe(
      (res) => {
        if (res instanceof HttpErrorResponse) {
          this.alertService.showWarningAlert('Lösung ungültig', 'Die eingegebene Lösung war ungültig. Versuchen sie es erneut');
        } else {
          this.riddleSolved.emit(this.riddle.id);
        }
      }
    );
  }

  /**
   * Is called then the user clicks on a file in the file list and initiates a download through the file service
   * @param file the file to be downloaded
   */
  downloadFile(file: File) {
    const result = this.fileService.download(this.riddle.id, file);
    this.progress = result.progress;
    this.progress.subscribe(
      newProgress => {
        if (newProgress === 100) {
          this.progress = undefined;
        }
      }
    );

    result.data.subscribe(
      data => {
        if (data.body instanceof Blob) {
          this.fileService.saveBlob(data.body, file.name);
        }
      }
    );
  }

}
