import { Component } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { FormGroup, FormControl } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-hunt-edit-modal',
  templateUrl: './hunt-edit-modal.component.html',
  styleUrls: ['./hunt-edit-modal.component.scss']
})
export class HuntEditModalComponent {
  huntEditForm = new FormGroup({
    title: new FormControl(),
    description: new FormControl(),
    reward: new FormControl(),
  });

  constructor(public modalController: ModalController, public alertService: AlertService, params: NavParams) {
    this.huntEditForm.controls.title.setValue(params.get('title'));
    this.huntEditForm.controls.description.setValue(params.get('description'));
    this.huntEditForm.controls.reward.setValue(params.get('reward'));
  }

  /**
   * Executed when submit button is pressed. Checks for valid input and hands it to the calling component when successful
   */
  onHuntEditFormSubmit() {
    // input validation
    const title = this.huntEditForm.controls.title.value;
    if (!title) {
      this.alertService.showWarningAlert('Schnitzeljagd bearbeiten', 'Bitte geben sie einen Titel an');
      return;
    }

    const description = this.huntEditForm.controls.description.value;
    if (!description) {
      this.alertService.showWarningAlert('Schnitzeljagd bearbeiten', 'Bitte geben sie eine Beschreibung an');
      return;
    }

    const reward = this.huntEditForm.controls.reward.value;
    if (!description) {
      this.alertService.showWarningAlert('Schnitzeljagd bearbeiten', 'Bitte geben sie eine Belohnung an');
      return;
    }

    // return input data to calling component
    this.modalController.dismiss({title, description, reward});
  }

  /**
   * Is called when hunt edit is cancelled
   */
  onHuntEditFormDismissed() {
    // dont return any data
    this.modalController.dismiss();
  }
}
