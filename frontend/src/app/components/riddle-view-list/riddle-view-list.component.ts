import { Component, OnInit, Input } from '@angular/core';
import { HuntDataService } from 'src/app/services/hunt-data.service';
import { Riddle } from '../../../../../backend/src/endpoints/responses';

@Component({
  selector: 'app-riddle-view-list',
  templateUrl: './riddle-view-list.component.html',
  styleUrls: ['./riddle-view-list.component.scss']
})
/**
 * Maintains a list of riddle-view components to be shown on the hunt-solve page
 */
export class RiddleViewListComponent implements OnInit {
  @Input() huntID: number;
  public solvedRiddles = new Array<Riddle>();

  constructor(public dataService: HuntDataService) { }

  /**
   * request new data form the backend
   */
  updateData(): void {
    this.dataService.getCompletedRiddles(this.huntID).subscribe(
      riddles => {
        this.solvedRiddles = riddles;
      }
    );
  }

  ngOnInit() {
    this.updateData();
  }

}
