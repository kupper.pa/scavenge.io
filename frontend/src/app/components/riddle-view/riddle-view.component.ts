import { Component, Input } from '@angular/core';
import { Riddle, File } from '../../../../../backend/src/endpoints/responses';
import { HuntDataService } from 'src/app/services/hunt-data.service';
import { FileService } from 'src/app/services/file.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-riddle-view',
  templateUrl: './riddle-view.component.html',
  styleUrls: ['./riddle-view.component.scss']
})
/**
 * Displays information about a riddle that has been completed by the user
 */
export class RiddleViewComponent {
  @Input() riddle: Riddle;

  progress: Observable<number>;

  constructor(public dataService: HuntDataService, public fileService: FileService) { }

  /**
   * Is called then the user clicks on a file in the file list and initiates a download through the file service
   * @param file the file to be downloaded
   */
  downloadFile(file: File) {
    const result = this.fileService.download(this.riddle.id, file);
    this.progress = result.progress;
    this.progress.subscribe(
      newProgress => {
        if (newProgress === 100) {
          this.progress = undefined;
        }
      }
    );

    result.data.subscribe(
      data => {
        if (data.body instanceof Blob) {
          this.fileService.saveBlob(data.body, file.name);
        }
      }
    );
  }
}
