import { Component, OnInit, ElementRef, ViewChild, Input, EventEmitter, Output } from '@angular/core';
import { FileService } from 'src/app/services/file.service';
import { Observable } from 'rxjs';
import { Riddle, File, Hint } from '../../../../../backend/src/endpoints/responses';
import { HuntDataService } from 'src/app/services/hunt-data.service';
import { ModalController } from '@ionic/angular';
import { HttpErrorResponse } from '@angular/common/http';
import { RiddleEditModalComponent } from '../riddle-edit-modal/riddle-edit-modal.component';
import { OverlayEventDetail } from '@ionic/core';
import { AlertService } from 'src/app/services/alert.service';
import { HintEditModalComponent } from 'src/app/components/hint-edit-modal/hint-edit-modal.component';

@Component({
  selector: 'app-riddle-edit',
  templateUrl: './riddle-edit.component.html',
  styleUrls: ['./riddle-edit.component.scss']
})
/**
 * Is displayed in the hunt edit page for each riddle the hunt contains and contains components for viewing editing a riddle
 */
export class RiddleEditComponent implements OnInit {
  @ViewChild('description', { read: ElementRef }) textArea: ElementRef;
  @ViewChild('file') file: ElementRef;

  @Output() riddleChangedEvent: EventEmitter<number> = new EventEmitter<number>();

  @Input() riddleID: number;
  riddle = { files: [], predecessors: [], possPredecessors: [] } as Riddle;

  progress: Observable<number>;

  predecessors = new Array<number>();

  constructor(public fileService: FileService, public dataService: HuntDataService, private alertService: AlertService, private modalController: ModalController) { }

  ngOnInit() {
    this.updateRiddle();
  }

  /**
   * request new data from the REST Api to update the contents of the component
   */
  updateRiddle() {
    this.dataService.getRiddle(this.riddleID).subscribe(
      riddle => {
        this.riddle = riddle;
        this.predecessors = riddle.predecessors.map(pred => pred.id);
      }
    );
  }

  /**
   * Is called when the user selects a new set of predecessors
   */
  onPredecessorSelectionChanged(newPredecessors: number[]) {
    this.dataService.setPredecessors(this.riddleID, { ids: newPredecessors }).subscribe(
      async res => {
        if (res instanceof HttpErrorResponse) {
          this.alertService.showWarningAlert('Fehler beim Bearbeiten', 'Die Abhängigkeiten konnten nicht bearbeitet werden. Liegt ein Zyklus vor?');
        }
        this.riddleChangedEvent.emit(this.riddle.id);
        this.updateRiddle();
      }
    );
  }

  /**
   * Is called when a file has been selected for upload. Uses the file upload service to upload the selected file
   */
  onFileSelected(): void {
    const file = this.file.nativeElement.files[0];
    this.progress = this.fileService.upload(file, this.riddleID);
    this.progress.subscribe(
      newProgress => {
        if (newProgress === 1) {
          this.progress = undefined;
          this.updateRiddle();
        }
      }
    );
  }

  /**
   * Requests a new title, description and solution for the Riddle and updates using the data service
   */
  async editTitleAndDescription() {
    // create confirmation dialog
    const modal = await this.modalController.create({
      component: RiddleEditModalComponent,
      componentProps: {
        title: this.riddle.title,
        description: this.riddle.description,
        solution: this.riddle.solution,
      },
      cssClass: 'riddle-edit-modal',
    });

    modal.onDidDismiss().then((eventDetail: OverlayEventDetail) => {
      const data = eventDetail.data;
      if (!data) { return; }
      // is executed if user confirms changes
      this.dataService.updateRidde(this.riddleID, { description: data.description, title: data.title, solution: data.solution }).subscribe(
        res => {
          if (res instanceof HttpErrorResponse) {
            // update failed
            this.onUpdateRiddleFailed();
          } else {
            // updated successful
            this.updateRiddle();
            this.riddleChangedEvent.emit(this.riddle.id);
          }
        }
      );
    });
    modal.present();
  }

  /**
   * Displays an error message if the riddle could not be edited correctly
   */
  async onUpdateRiddleFailed() {
    this.alertService.showWarningAlert('Fehler beim Bearbeiten', 'Das Rätsel konnte aus einem unbekannten Grund nicht bearbeitet werden. Versuchen sie es bitte erneut.');
  }

  /**
   * Is called when the select file button is pressed and simulates a click on the file input
   */
  selectFile(): void {
    this.file.nativeElement.click();
  }

  /**
   * Is called when the remove riddle button is clicked and removes the riddle form the hunt using the data Sevice
   */
  async removeRiddle() {
    const confirmation = await this.alertService.showConfirmationAlert('Rätsel löschen', 'Wollen sie die das Rätsel wirklich löschen?', 'Löschen');
    if (confirmation) {
      // is executed if user confirms deletion
      this.dataService.removeRiddle(this.riddle.id).subscribe(
        () => this.riddleChangedEvent.emit(this.riddle.id)
      );
    }
  }

  /**
   * Is called then the user clicks on a file in the file list and initiates a download through the file service
   * @param file the file to be downloaded
   */
  downloadFile(file: File) {
    const result = this.fileService.download(this.riddle.id, file);
    this.progress = result.progress;
    this.progress.subscribe(
      newProgress => {
        if (newProgress === 1) {
          this.progress = undefined;
        }
      }
    );

    result.data.subscribe(
      data => {
        if (data.body instanceof Blob) {
          this.fileService.saveBlob(data.body, file.name);
        }
      }
    );
  }

  /**
   * Is called when the user clicks on the delete button next to a file and deletes the file from the riddle using the file service
   * @param file the file to be deleted
   */
  deleteFile(file: File) {
    this.fileService.remove(this.riddle.id, file).subscribe(
      res => {
        if (res instanceof HttpErrorResponse) {
          console.error(res);
        } else {
          this.updateRiddle();
        }
      }
    );
  }

  /**
   * Is called when the user clicks on the add Hint button
   */
  async addHint() {
    // create riddle dialog
    const modal = await this.modalController.create({
      component: HintEditModalComponent,
      componentProps: {
        title: '',
        description: '',
        displayInHours: '',
      },
      cssClass: 'hint-edit-modal',
    });

    modal.onDidDismiss().then((eventDetail: OverlayEventDetail) => {
      const data = eventDetail.data;
      if (!data) return;
      // is executed if user confirm riddle creation
      this.dataService.addHint(this.riddle.id, data).subscribe(
        res => {
          if (res instanceof HttpErrorResponse)
            console.log(res.error);
          else {
            this.updateRiddle();
          }
        }
      );
    });
    modal.present();
  }

  /**
   * Is called when the user clicks on a Hint
   */
  async editHint(hint: Hint) {
    // create confirmation dialog
    const modal = await this.modalController.create({
      component: HintEditModalComponent,
      componentProps: {
        title: hint.title,
        description: hint.description
      },
      cssClass: 'hint-edit-modal',
    });

    modal.onDidDismiss().then((eventDetail: OverlayEventDetail) => {
      const data = eventDetail.data;
      if (!data) { return; }
      // is executed if user confirms changes
      this.dataService.updateHint(hint.id, { description: data.description, title: data.title } as Hint).subscribe(
        res => {
          if (res instanceof HttpErrorResponse)
            this.alertService.showWarningAlert('Fehler beim Bearbeiten', 'Der Hinweis konnte nicht bearbeitet werden.');
          else
            this.updateRiddle();
        }
      );
    });
    modal.present();
  }

  /**
   * Is called when the user clicks on the delete button next to a hint and deletes the hint from the riddle
   * @param file the file to be deleted
   */
  deleteHint(hint: Hint) {
    this.dataService.deleteHint(hint.id).subscribe(
      res => {
        if (res instanceof HttpErrorResponse)
          this.alertService.showWarningAlert('Fehler beim Löschen', 'Der Hinweis konnte nicht gelöscht werden.');
        else
          this.updateRiddle();
      }
    );
  }
}
