import { Component, Input, Output, EventEmitter, ViewChildren, QueryList } from '@angular/core';
import { RiddleEditComponent } from '../riddle-edit/riddle-edit.component';

@Component({
  selector: 'app-riddle-edit-list',
  templateUrl: './riddle-edit-list.component.html',
  styleUrls: ['./riddle-edit-list.component.scss']
})
/**
 * Maintains a number of riddle-edit components to be shown on the edit hunt page
 */
export class RiddleEditListComponent {
  @Input() riddles: number[];
  @Output() riddleDataChangedEvent = new EventEmitter<number>();

  @ViewChildren(RiddleEditComponent) private editComponents: QueryList<RiddleEditComponent>;

  /**
   * Is called by a riddle-edit child component when the corresponding riddle has been deleted
   * @param riddleID the riddle that was deleted
   */
  onRiddleChanged(riddleID: number) {
    this.riddleDataChangedEvent.emit(riddleID);
  }

  updatePredecessors() {
    this.editComponents.forEach(elem => {
      elem.updateRiddle();
    });
  }
}
