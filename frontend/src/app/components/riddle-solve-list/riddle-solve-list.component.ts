import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { HuntDataService } from 'src/app/services/hunt-data.service';
import { Riddle } from '../../../../../backend/src/endpoints/responses';

@Component({
  selector: 'app-riddle-solve-list',
  templateUrl: './riddle-solve-list.component.html',
  styleUrls: ['./riddle-solve-list.component.scss']
})
/**
 * Maintains a list of riddle-solve components to be shown on the hunt solve page
 */
export class RiddleSolveListComponent implements OnInit {
  @Input() huntID: number;

  @Output() riddleSolved: EventEmitter<number> = new EventEmitter<number>();

  public openRiddles = new Array<Riddle>();

  constructor(public dataService: HuntDataService) { }

  /**
   * request new data form the backend
   */
  updateData(): void {
    this.dataService.getActiveRiddles(this.huntID).subscribe(
      riddles => {
        this.openRiddles = riddles;
      }
    );
  }

  /**
   * Is called when a riddle is solved. Reports the change to the parent component
   * @param riddleID the ID of the solved riddle
   */
  onRiddleSolvedEvent(riddleID: number) {
    for (let i = 0; i < this.openRiddles.length; i++) {
      if (this.openRiddles[i].id === riddleID) {
        this.openRiddles.splice(i, 1);
      }
    }
    this.riddleSolved.emit(riddleID);
  }

  ngOnInit() {
    this.updateData();
  }

}
