import { Component, Input, OnInit } from '@angular/core';
import { Hunt } from '../../../../../backend/src/endpoints/responses';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hunts-table-solve-entry',
  templateUrl: './hunts-table-solve-entry.component.html',
  styleUrls: ['./hunts-table-solve-entry.component.scss']
})
/**
 * Is shown on the hunts overview page for each solvable hunt
 */
export class HuntsTableSolveEntryComponent implements OnInit {
  @Input() hunt: Hunt;
  editors: string;

  constructor(private router: Router) { }

  ngOnInit() {
    this.editors = this.hunt.editors.map(e => e.username).join(', ');
  }

  /**
   * Is called when clicking on the solve button and navigates
   * to the solve page for the hunt corresponding to this table entry
   */
  solveHunt() {
    this.router.navigate(['hunt/solve', this.hunt.id]);
  }

}
