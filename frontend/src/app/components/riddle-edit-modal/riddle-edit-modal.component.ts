import { Component } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { FormGroup, FormControl } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-riddle-edit-modal',
  templateUrl: './riddle-edit-modal.component.html',
  styleUrls: ['./riddle-edit-modal.component.scss']
})
/**
 * contains a modal popup, that shall be displayed when a riddle is edited (currently not used)
 */
export class RiddleEditModalComponent {

  riddleEditForm = new FormGroup({
    title: new FormControl(),
    description: new FormControl(),
    solution: new FormControl(),
  });

  constructor(public modalController: ModalController, public alertService: AlertService, params: NavParams) {
    this.riddleEditForm.controls.title.setValue(params.get('title'));
    this.riddleEditForm.controls.description.setValue(params.get('description'));
    this.riddleEditForm.controls.solution.setValue(params.get('solution'));
  }

  /**
   * Executed when submit button is pressed. Checks for valid input and hands it to the calling component when successful
   */
  onRiddleEditFormSubmit() {
    // input validation
    const title = this.riddleEditForm.controls.title.value;
    if (!title) {
      this.alertService.showWarningAlert('Rätsel bearbeiten', 'Bitte geben sie einen Titel an');
      return;
    }

    const description = this.riddleEditForm.controls.description.value;
    if (!description) {
      this.alertService.showWarningAlert('Rätsel bearbeiten', 'Bitte geben sie eine Beschreibung an');
      return;
    }

    const solution = this.riddleEditForm.controls.solution.value;
    if (!solution) {
      this.alertService.showWarningAlert('Rätsel bearbeiten', 'Bitte geben sie eine Lösung an');
      return;
    }

    // return input data to calling component
    this.modalController.dismiss({ title, description, solution });
  }

  /**
   * Is called when riddle edit is cancelled
   */
  onRiddleEditFormDismissed() {
    // dont return any data
    this.modalController.dismiss();
  }
}
