import { Component, Input } from '@angular/core';
import { Hunt } from '../../../../../backend/src/endpoints/responses';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hunts-table-edit-entry',
  templateUrl: './hunts-table-edit-entry.component.html',
  styleUrls: ['./hunts-table-edit-entry.component.scss']
})
/**
 * Is shown on the hunts overview page for each editable hunt
 */
export class HuntsTableEditEntryComponent {
  @Input() hunt: Hunt;

  constructor(private router: Router) { }

  /**
   * Is called when clicking on the edit button and navigates
   * to the edit page for the hunt corresponding to this table entry
   */
  editHunt() {
    this.router.navigate(['hunt/edit', this.hunt.id]);
  }

}
