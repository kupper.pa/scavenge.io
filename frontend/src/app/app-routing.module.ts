import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundViewComponent } from './views/page-not-found-view/page-not-found-view.component';
import { HomeViewComponent } from './views/home-view/home-view.component';
import { HuntsViewComponent } from './views/hunts-view/hunts-view.component';
import { AboutViewComponent } from './views/about-view/about-view.component';
import { HuntEditViewComponent } from './views/hunt-edit-view/hunt-edit-view.component';
import { HuntSolveViewComponent } from './views/hunt-solve-view/hunt-solve-view.component';
import { LoginViewComponent } from './views/login-view/login-view.component';
import { ProfileViewComponent } from './views/profile-view/profile-view.component';
import { AuthGuardService } from './services/auth-guard.service';
import { RegisterViewComponent } from './views/register-view/register-view.component';
import { AuthInterceptorService } from './services/auth-interceptor.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HuntCreateViewComponent } from './views/hunt-create-view/hunt-create-view.component';
import { ContactViewComponent } from './views/contact-view/contact-view.component';
import { ServerErrorViewComponent } from './views/server-error-view/server-error-view.component';
import { SolveViewComponent } from './views/solve-view/solve-view.component';
import { TutorialViewComponent } from './views/tutorial-view/tutorial-view.component';

const routes: Routes = [
  { path: '', component: HomeViewComponent },
  { path: 'about', component: AboutViewComponent },
  { path: 'tutorial', component: TutorialViewComponent },
  { path: 'contact', component: ContactViewComponent },
  { path: 'hunts', component: HuntsViewComponent, canActivate: [AuthGuardService] },
  { path: 'profile', component: ProfileViewComponent, canActivate: [AuthGuardService] },
  { path: 'login', component: LoginViewComponent },
  { path: 'register', component: RegisterViewComponent },
  { path: 'solve/:id/:solution', component: SolveViewComponent, canActivate: [AuthGuardService] },
  { path: 'hunt/create', component: HuntCreateViewComponent, canActivate: [AuthGuardService] },
  { path: 'hunt/edit/:id', component: HuntEditViewComponent, canActivate: [AuthGuardService] },
  { path: 'hunt/solve/:id', component: HuntSolveViewComponent, canActivate: [AuthGuardService] },
  { path: 'servererror', component: ServerErrorViewComponent},
  { path: '**', component: PageNotFoundViewComponent }
];

@NgModule({
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
  ],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
