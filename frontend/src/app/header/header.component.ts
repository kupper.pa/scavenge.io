import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
/**
 * Contains the header of the application including title and navigation
 */
export class HeaderComponent {

  constructor(public authService: AuthService, public apiService: ApiService) { }

}
