import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Me, User, Hunt, Hunts } from '../../../../backend/src/endpoints/responses';
import { RevokeToken, Register, Password, PasswordChange, AddHunt } from '../../../../backend/src/endpoints/requests';

const endpoint = '/api/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
/**
 * Provides functions to acces REST Endpoints
 */
export class ApiService {
  constructor(private http: HttpClient) { }

  /**
   * Request information about a user from the backend
   * @param userId the user whose information is requested
   */
  requestUserInfo(userId: number): Observable<User> {
    return this.http.get(endpoint + 'user/' + userId, httpOptions).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Requests information about the logged in user from the backend
   */
  requestMeInfo(): Observable<Me> {
    return this.http.get(endpoint + 'me', httpOptions).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Invalidates the session of the currently logged in user
   * @param parameters the current access and refresh tokens
   */
  logoutUser(parameters: RevokeToken): Observable<HttpErrorResponse> {
    return this.http.post(endpoint + 'user/logout', JSON.stringify(parameters), httpOptions).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Calls the endpoint to create a new user
   * @param parameters the data required to register a new user
   */
  registerUser(parameters: Register): Observable<HttpErrorResponse> {
    return this.http.post(endpoint + 'user/register', JSON.stringify(parameters), httpOptions).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Deletes the currently logged in user through the corresponding endpoint
   * @param parameters contains the password of the logged in user as confirmation
   */
  removeUser(parameters: Password): Observable<HttpErrorResponse> {
    // default delete funtion does not allow request body
    const options = httpOptions;
    (options as any).body = parameters;
    return this.http.request('DELETE', endpoint + 'user/remove', options).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Changes the password for the currently logged in user
   * @param parameters contains the old password of the logged in user as confirmation and the new password
   */
  changePassword(parameters: PasswordChange): Observable<HttpErrorResponse> {
    return this.http.put(endpoint + 'user/password', JSON.stringify(parameters), httpOptions).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Adds a new hunt with the given parameters and makes the currently logged in user an editor for that hunt
   * @param parameters the data required for hunt creation
   */
  addHunt(parameters: AddHunt): Observable<Hunt> {
    return this.http.post(endpoint + 'hunt', JSON.stringify(parameters), httpOptions).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Requests a list of hunts created for the user
   */
  getReceivedHunts(): Observable<Hunts> {
    return this.http.get(endpoint + 'hunt/received', httpOptions).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Request a list of hunts editable by the user
   */
  getCreatedHunts(): Observable<Hunts> {
    return this.http.get(endpoint + 'hunt/created', httpOptions).pipe(
      catchError(
        error => of(error)
      )
    );
  }
}
