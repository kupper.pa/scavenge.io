import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
/**
 * Responsible for keeping a user form visiting protected pages when not logged in
 */
export class AuthGuardService implements CanActivate {
  constructor(private authService: AuthService, private router: Router) { }

  /**
   * Returns wether the user is allowed to acces a protected route
   * @param route the currently active route
   * @param state the state of the router
   * @returns wether the route can be activated
   */
  canActivate(_: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.authService.isSessionRecoverable()) {
      return true;
    } else {
      this.router.navigate(['login'], { queryParams: { returnUrl: state.url } });
      return false;
    }
  }
}
