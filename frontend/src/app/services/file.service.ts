import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEventType, HttpResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { File as FileRes } from '../../../../backend/src/endpoints/responses';
import { catchError } from 'rxjs/operators';
import { AlertService } from './alert.service';

const endpoint = '/api/riddle/';

@Injectable({
  providedIn: 'root'
})
/**
 * Provides access to the file endpoints
 */
export class FileService {
  constructor(private http: HttpClient, public alertService: AlertService) { }
  /**
   * Uploads a file and attaches it to a riddle
   * @param file the file to be uploaded
   * @param riddleId the riddle the file will be attached to
   */
  public upload(file: File, riddleId: number): Observable<number> {
    if (file.size > 1024 * 1024 * 1024) { // 1 GiB
      this.alertService.showWarningAlert('Fehler beim Datei Upload', 'Die maximale Dateigröße wurde überschritten.');
      return null;
    }
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);

    const req = new HttpRequest('POST', endpoint + riddleId + '/file', formData, {
      reportProgress: true
    });

    const progress = new Subject<number>();

    this.http.request(req).subscribe(event => {
      if (event instanceof HttpErrorResponse) {
        if (event.status === 403) {
          this.alertService.showWarningAlert('Fehler beim Datei Upload', 'Die maximale Anzahl an Dateien wurde überschritten');
        } else {
          this.alertService.showWarningAlert('Fehler beim Datei Upload', 'Ein unbekannter Fehler ist aufgetreten. Bitte versuchen sie es erneut');
        }
        return;
      }
      if (event.type === HttpEventType.UploadProgress) {
        const percentDone = event.loaded / event.total;
        progress.next(percentDone);
      } else if (event instanceof HttpResponse) {
        progress.complete();
      }
    });

    return progress.asObservable();
  }

  /**
   * Downloads a file attached to a riddle
   * @param riddleId the ID of the riddle the file is attached to
   * @param file the file to be downloaded
   */
  public download(riddleId: number, file: FileRes) {
    const req = new HttpRequest('GET', endpoint + riddleId + '/file/' + file.hash, {
      responseType: 'blob',
      reportProgress: true
    });

    const progress = new Subject<number>();
    const data = new Subject<HttpResponse<{}>>();

    this.http.request(req).subscribe(event => {
      if (event.type === HttpEventType.DownloadProgress) {
        if (event instanceof HttpErrorResponse) {
          this.alertService.showWarningAlert('Fehler beim Datei Download', 'Ein unbekannter Fehler ist aufgetreten. Bitte versuchen sie es erneut');
        }
        const percentDone = event.loaded / event.total;
        progress.next(percentDone);
      } else if (event instanceof HttpResponse) {
        data.next(event);
        data.complete();
        progress.complete();
      }
    });

    return { data: data.asObservable(), progress: progress.asObservable() };
  }

  /**
   * Deletes a file from the server
   * @param riddleID the riddle the file is attached to
   * @param file the file to be deleted
   */
  public remove(riddleID: number, file: FileRes) {
    return this.http.delete(endpoint + riddleID + '/file/' + file.hash, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Downloads a blob as a file to disk.
   * @param data blob containing the file to be saved
   */
  saveBlob(data: Blob, filename: string) {
    // For Internet Explorer
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(data, filename);
      return;
    }

    // For other browsers:
    // Create a link pointing to the ObjectURL containing the blob.
    const downloadURL = window.URL.createObjectURL(data);
    const link = document.createElement('a');
    document.body.appendChild(link);
    link.href = downloadURL;
    link.download = filename;
    link.click();
  }
}
