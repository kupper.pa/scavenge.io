import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(private alertController: AlertController) { }

  async showWarningAlert(title: string, message: string) {
    // create info dialog
    const alert = await this.alertController.create({
      header: title,
      message,
      buttons: [
        {
          text: 'Ok',
          role: 'cancel',
        }
      ]
    });
    // show confirmation dialog
    alert.present();
  }

  async showConfirmationAlert(title: string, message: string, action: string): Promise<boolean> {
    const alert = await this.alertController.create({
      header: title,
      message,
      buttons: [
        {
          text: 'Abbrechen',
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: action,
        }
      ]
    });

    // show confirmation dialog
    alert.present();
    const data = await alert.onDidDismiss();
    return data.role !== 'cancel';
  }

  async showPasswordConfirmationAlert(title: string, message: string, action: string): Promise<string> {
    // create confirmation dialog
    const alert = await this.alertController.create({
      header: title,
      message,
      inputs: [
        {
          name: 'password',
          placeholder: 'Passwort',
          type: 'password'
        }
      ],
      buttons: [
        {
          text: 'Abbrechen',
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: action
        }
      ]
    });
    // show confirmation dialog
    alert.present();
    const data = await alert.onDidDismiss();
    return data.role === 'cancel' ? null : data.data.values.password;
  }

  async showPasswordChangeAlert(): Promise<{ oldPassword: string, password: string, passwordRepeat: string }> {
    // create confirmation dialog
    const alert = await this.alertController.create({
      header: 'Passwort ändern',
      message: 'Geben Sie zum Ändern des Passworts das alte und ein neues Passwort ein:',
      inputs: [
        {
          name: 'oldPassword',
          placeholder: 'Altes Passwort',
          type: 'password'
        },
        {
          name: 'password',
          placeholder: 'Neues Passwort',
          type: 'password'
        },
        {
          name: 'passwordRepeat',
          placeholder: 'Neues Passwort',
          type: 'password'
        }
      ],
      buttons: [
        {
          text: 'Abbrechen',
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: 'Ändern'
        }
      ]
    });
    // show confirmation dialog
    alert.present();
    const data = await alert.onDidDismiss();
    const { oldPassword, password, passwordRepeat } = data.data.values;
    return data.role === 'cancel' ? null : { oldPassword, password, passwordRepeat };
  }
}

