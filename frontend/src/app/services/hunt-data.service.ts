import { Injectable } from '@angular/core';
import { Hunt, Riddle, Hint } from '../../../../backend/src/endpoints/responses';
import { EditHunt, AddRiddle, Solution, IDArray, SearchBody, User } from '../../../../backend/src/endpoints/requests';
import { Observable, of } from 'rxjs';
import { HttpErrorResponse, HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

const huntEndpoint = '/api/hunt';
const riddleEndpoint = '/api/riddle';
const hintEndpoint = '/api/hint';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
/**
 * Provides access to the hunt-related REST endpoints
 */
export class HuntDataService {

  constructor(private http: HttpClient) { }

  /**
   * Updates a given hunt with new parameters
   * @param huntID the hunt to be updated
   * @param parameters the new hunt parameters
   */
  updateHunt(huntID: number, parameters: EditHunt): Observable<Hunt> {
    return this.http.put(huntEndpoint + '/' + huntID, JSON.stringify(parameters), httpOptions).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Deletes a hunt
   */
  removeHunt(huntID: number): Observable<HttpErrorResponse> {
    return this.http.delete(huntEndpoint + '/' + huntID, httpOptions).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Requests information about a specific hunt
   * @param huntID the ID of the hunt to be requested
   */
  getHunt(huntID: number): Observable<Hunt> {
    return this.http.get(huntEndpoint + '/' + huntID, httpOptions).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Requests riddles belonging to a specific hunt
   * @param huntID the ID of the hunt
   */
  getRiddles(huntID: number): Observable<Riddle[]> {
    return this.http.get(huntEndpoint + '/' + huntID + '/riddles', httpOptions).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Requests riddles which can currently be solved
   * @param huntID the ID of the hunt
   */
  getActiveRiddles(huntID: number): Observable<Riddle[]> {
    return this.http.get(huntEndpoint + '/' + huntID + '/active?expand=1', httpOptions).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Requests riddles which are completed
   * @param huntID the ID of the hunt
   */
  getCompletedRiddles(huntID: number): Observable<Riddle[]> {
    return this.http.get(huntEndpoint + '/' + huntID + '/completed?expand=1', httpOptions).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Creates a new Riddle and attaches it to an existing hunt
   * @param huntID the id of the hunt the riddle will be attached to
   * @param parameters the riddle parameters
   */
  addRiddle(huntID: number, parameters: AddRiddle): Observable<Riddle> {
    return this.http.post(huntEndpoint + '/' + huntID + '/riddle', JSON.stringify(parameters), httpOptions).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Updates an existing riddle with new data
   * @param riddleID the ID of the riddle to be updated
   * @param parameters the new riddle parameters
   */
  updateRidde(riddleID: number, parameters: AddRiddle): Observable<HttpErrorResponse> {
    return this.http.put(riddleEndpoint + '/' + riddleID, JSON.stringify(parameters), httpOptions).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Requests data about an existing riddle
   * @param riddleID the id of the riddle to request
   */
  getRiddle(riddleID: number): Observable<Riddle> {
    return this.http.get<Riddle>(riddleEndpoint + '/' + riddleID, httpOptions).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Solves a riddle
   * @param riddleID the ID of the riddle to be solved
   * @param solution the solution of the riddle
   */
  solveRiddle(riddleID: number, solution: Solution): Observable<HttpErrorResponse> {
    return this.http.post(riddleEndpoint + '/' + riddleID + '/solve', JSON.stringify(solution), httpOptions).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Deletes an existing riddle
   * @param riddleID the ID of the riddle to be deleted
   */
  removeRiddle(riddleID: number): Observable<HttpErrorResponse> {
    return this.http.delete(riddleEndpoint + '/' + riddleID, httpOptions).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Sets the predecessors of a given hunt
   * @param riddleID the ID of the riddle
   * @param predecessors the new predecessors for the riddle
   */
  setPredecessors(riddleID: number, predecessors: IDArray) {
    return this.http.put(riddleEndpoint + '/' + riddleID + '/predecessor', JSON.stringify(predecessors), httpOptions).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Requests a graph of riddles from the specified hunt
   * @param huntID the ID of the hunt
   */
  getHuntGraph(huntID: number) {
    return this.http.get(huntEndpoint + '/' + huntID + '/graph', httpOptions).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Adds a user as an editor of a specified hunt
   * @param huntID the ID of the hunt
   * @param editor the Search parameters for the user
   */
  addHuntEditor(huntID: number, editor: SearchBody) {
    return this.http.post(huntEndpoint + '/' + huntID + '/editor', JSON.stringify(editor), httpOptions).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Removes editor privileges for a specified hunt from a user
   * @param huntID the ID of the hunt
   * @param editor: the editor to be removed
   */
  removeHuntEditor(huntID: number, editor: User) {
    // Note: http.delete does not allow request body
    const options = httpOptions;
    (options as any).body = editor;
    return this.http.request('DELETE', huntEndpoint + '/' + huntID + '/editor', options).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Adds a hint
   */
  addHint(riddleId: number, parameters: Hint): Observable<HttpErrorResponse>  {
    return this.http.post(riddleEndpoint + '/' + riddleId + '/hint', JSON.stringify(parameters), httpOptions).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Updates a hint
   */
  updateHint(hintId: number, parameters: Hint): Observable<HttpErrorResponse>  {
    return this.http.put(hintEndpoint + '/' + hintId, JSON.stringify(parameters), httpOptions).pipe(
      catchError(
        error => of(error)
      )
    );
  }

  /**
   * Deletes a hint
   */
  deleteHint(hintId: number): Observable<HttpErrorResponse>  {
    return this.http.delete(hintEndpoint + '/' + hintId, httpOptions).pipe(
      catchError(
        error => of(error)
      )
    );
  }
}
