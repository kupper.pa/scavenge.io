import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError, filter, take, switchMap } from 'rxjs/operators';
import { ErrorHandlerService } from './error-handler.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
/**
 * Intercepts outgoing HTTP requests and adds session data for authentication
 */
export class AuthInterceptorService implements HttpInterceptor {

  private refreshTokenInProgress = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(public authService: AuthService, public errorHandler: ErrorHandlerService, private router: Router) { }

  /**
   * I called when a HTTP request ist sent. Can modify, block or replace the request.
   * Adds authentication data when needed and handles server errors
   * Refreshes authentication data when necessary
   * @param req the request to be sent
   * @param next the next handler to be called
   */
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    req = this.addAccessToken(req);
    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {

        if (req.url.includes('login') || req.url.includes('register') || req.url.includes('logout')) {
          return throwError(error);
        }

        // handle server errors
        if (error.status >= 500 && error.status < 600) {
          this.router.navigateByUrl('/servererror', { skipLocationChange: true });
          return throwError(error);
        }

        if (error.status !== 401) {
          return throwError(error);
        }

        if (this.refreshTokenInProgress) {
          return this.refreshTokenSubject.pipe(
            filter(result => result != null),
            take(1),
            switchMap(() => next.handle(this.addAccessToken(req)))
          );
        } else {
          this.refreshTokenInProgress = true;
          this.refreshTokenSubject.next(null);

          return this.authService.refreshSession().pipe(
            switchMap((token: any) => {
              this.refreshTokenInProgress = false;
              this.refreshTokenSubject.next(token);
              return next.handle(this.addAccessToken(req));
            }),
            catchError(() => {
              this.refreshTokenInProgress = false;
              this.authService.endSession();
              return throwError(error);
            }));
        }
      })
    );
  }

  /**
   * Adds authentication data to a given HTTP request
   * @param request the unmodified HTTP request
   * @returns the moified HTTP request
   */
  private addAccessToken(request: HttpRequest<any>): HttpRequest<any> {
    const accessToken = this.authService.getAccessToken();

    if (!accessToken) {
      return request;
    }

    return request.clone({
      setHeaders: {
        Authorization: `Bearer ${accessToken}`
      }
    });
  }
}
