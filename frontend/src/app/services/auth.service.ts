import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { tap, mapTo, catchError } from 'rxjs/operators';
import { ApiService } from './api.service';

interface AuthData {
  accessToken: string;
  refreshToken: string;
  accessTokenExpiresAt: string;
  refreshTokenExpiresAt: string;
}

/**
 * persistently stores the session data in the users browser to stay logged in after page reload
 */
class SessionDataManager {
  private static readonly usernameKey: string = 'username';
  private static readonly accessTokenKey: string = 'accessToken';
  private static readonly refreshTokenKey: string = 'refreshToken';
  private static readonly accessTokenExpiresAtKey: string = 'accessTokenExpiresAt';
  private static readonly refreshTokenExpiresAtKey: string = 'refreshTokenExpiresAt';

  static getAccessToken = () => localStorage.getItem(SessionDataManager.accessTokenKey);
  static getRefreshToken = () => localStorage.getItem(SessionDataManager.refreshTokenKey);

  static setUsername(username: string): void {
    localStorage.setItem(SessionDataManager.usernameKey, username);
  }

  static getUsername(): string {
    return localStorage.getItem(SessionDataManager.usernameKey);
  }

  static getAccessTokenExpiresAt(): Date {
    const accessTokenExpiresAt = localStorage.getItem(SessionDataManager.accessTokenExpiresAtKey);
    return accessTokenExpiresAt ? new Date(accessTokenExpiresAt) : null;
  }
  static getRefreshTokenExpiresAt(): Date {
    const refreshTokenExpiresAt = localStorage.getItem(SessionDataManager.refreshTokenExpiresAtKey);
    return refreshTokenExpiresAt ? new Date(refreshTokenExpiresAt) : null;
  }

  static setSessionData(data: AuthData): void {
    localStorage.setItem(SessionDataManager.accessTokenKey, data.accessToken);
    localStorage.setItem(SessionDataManager.refreshTokenKey, data.refreshToken);
    localStorage.setItem(SessionDataManager.accessTokenExpiresAtKey, data.accessTokenExpiresAt);
    localStorage.setItem(SessionDataManager.refreshTokenExpiresAtKey, data.refreshTokenExpiresAt);
  }

  static clearSessionData(): void {
    localStorage.removeItem(SessionDataManager.accessTokenKey);
    localStorage.removeItem(SessionDataManager.refreshTokenKey);
    localStorage.removeItem(SessionDataManager.accessTokenExpiresAtKey);
    localStorage.removeItem(SessionDataManager.refreshTokenExpiresAtKey);
  }
}

@Injectable({
  providedIn: 'root'
})
/**
 * Manages session data an calls authentication endpoints
 */
export class AuthService {
  private httpOptions = { headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded') };
  private oauthEndpoint = '/api/user/login';

  constructor(private http: HttpClient, private router: Router, public apiService: ApiService) { }


  /**
   * Requests new session data from the backend
   * @param username the username of the user to be logged in
   * @param password the password of the user to be logged in
   */
  startSession(username: string, password: string): Observable<boolean | HttpErrorResponse> {
    SessionDataManager.setUsername(username);

    const body: string = 'grant_type=password'
      + '&client_id=scavengeio'
      + '&client_secret=secret'
      + `&username=${username}`
      + `&password=${password}`;

    return this.http.post<AuthData>(this.oauthEndpoint, body, this.httpOptions).pipe(
      tap(data => SessionDataManager.setSessionData(data)),
      mapTo(true),
      catchError(error => of(error))
    );
  }

  /**
   * Requests updated session data when the access token has been expired
   */
  refreshSession(): Observable<boolean | HttpErrorResponse> {
    const body: string = 'grant_type=refresh_token'
      + '&client_id=scavengeio'
      + '&client_secret=secret'
      + `&refresh_token=${SessionDataManager.getRefreshToken()}`;

    return this.http.post<AuthData>(this.oauthEndpoint, body, this.httpOptions).pipe(
      tap(data => SessionDataManager.setSessionData(data)),
      mapTo(true),
      catchError(error => of(error))
    );
  }

  /**
   * Deletes session data from both client and server
   */
  endSession() {
    this.apiService.logoutUser({ accessToken: SessionDataManager.getAccessToken(), refreshToken: SessionDataManager.getRefreshToken() }).subscribe();
    SessionDataManager.clearSessionData();
    this.router.navigate(['login'], { queryParams: {returnUrl: this.router.url }});
  }

  /**
   * @returns whether the session is valid or can be reactivated using the refresh token
   */
  isSessionRecoverable = () =>
    this.isSessionValid() || this.isRefreshTokenValid()


  /**
   * @returns whether the session is valid
   */
  isSessionValid(): boolean {
    const accessTokenExpiresAt = SessionDataManager.getAccessTokenExpiresAt();
    if (accessTokenExpiresAt == null) { return false; }
    return accessTokenExpiresAt > new Date();
  }

  /**
   * @returns whether the session can be reactivated using the refresh token
   */
  isRefreshTokenValid(): boolean {
    const refreshTokenExpiresAt = SessionDataManager.getRefreshTokenExpiresAt();
    if (refreshTokenExpiresAt == null) { return false; }
    return refreshTokenExpiresAt > new Date();
  }

  /**
   * @returns the current access token
   */
  getAccessToken = () =>
    SessionDataManager.getAccessToken()

  /**
   * @returns the current refresh token
   */
  getUsername = () =>
    SessionDataManager.getUsername()
}


