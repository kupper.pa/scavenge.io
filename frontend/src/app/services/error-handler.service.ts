import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
/**
 * Currently unused, reserved for future updates
 * handles an HTTP error
 */
export class ErrorHandlerService {

  constructor(public authService: AuthService) { }

  public handleHttpError(error: HttpErrorResponse) {
    const errorData = {
      code: error.error.code,
      message: error.error.message
    };
    if (errorData.code === 401) {
      this.authService.refreshSession();
    }
  }
}
