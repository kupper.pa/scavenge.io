import { Component, OnInit, ViewChild } from '@angular/core';
import { Hunt } from '../../../../../backend/src/endpoints/responses';
import { HuntDataService } from 'src/app/services/hunt-data.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { RiddleViewListComponent } from 'src/app/components/riddle-view-list/riddle-view-list.component';
import { EventHandler } from '@ionic/angular/dist/providers/events';
import { RiddleSolveListComponent } from 'src/app/components/riddle-solve-list/riddle-solve-list.component';
import { HuntGraphComponent } from 'src/app/components/hunt-graph/hunt-graph.component';

@Component({
  selector: 'app-hunt-solve-view',
  templateUrl: './hunt-solve-view.component.html',
  styleUrls: ['./hunt-solve-view.component.scss']
})
/**
 * Contains information and controls necessary to solve a hunt
 */
export class HuntSolveViewComponent implements OnInit {
  huntID: number;
  hunt = {} as Hunt;
  editors = '';
  openRiddles = new Array<number>();
  solvedRiddles = new Array<number>();

  @ViewChild(RiddleViewListComponent) viewList: RiddleViewListComponent;
  @ViewChild(RiddleSolveListComponent) solveList: RiddleSolveListComponent;
  @ViewChild(HuntGraphComponent) graph: HuntGraphComponent;

  constructor(public dataService: HuntDataService, private route: ActivatedRoute, public alertController: AlertController) { }

  /**
   * request new data form the backend
   */
  updateData(): void {
    this.dataService.getHunt(this.huntID).subscribe(
      hunt => {
        this.hunt = hunt;
        this.editors = hunt.editors.map(e => e.username).join(', ');
      }
    );
  }

  /**
   * Updates the solved riddle list when a riddle has been reported to be solved
   */
  onRiddleSolved = (_: EventHandler) => {
    this.updateData();
    this.viewList.updateData();
    this.solveList.updateData();
    this.graph.updateData();
  }


  ngOnInit() {
    this.route.paramMap.subscribe(
      (params: ParamMap) => {
        this.huntID = +params.get('id');
        this.updateData();
      }
    );
  }
}
