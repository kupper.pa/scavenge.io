import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { validateEmail, validateUsername } from 'src/app/util/input-validation';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-login-view',
  templateUrl: './login-view.component.html',
  styleUrls: ['./login-view.component.scss']
})
/**
 * Provides an interface to start a new session
 */
export class LoginViewComponent implements OnInit {
  loginForm = new FormGroup({
    username: new FormControl(),
    password: new FormControl()
  });

  returnUrl: string;

  constructor(public authService: AuthService, private router: Router, private route: ActivatedRoute, public alertService: AlertService) { }

  ngOnInit(): void {
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  /**
   * ist called when the login form is submitted. Validates the input and calls the authentication service
   */
  onLoginFormSubmit(): void {
    // input validation
    const username = this.loginForm.controls.username.value;
    if (!username || !(validateUsername(username) || validateEmail(username))) {
      this.alertService.showWarningAlert('Login Fehler', 'Bitte geben sie einen gültigen Nutzernamen oder eine Gültige Email-Adresse ein.');
      return;
    }
    if (username.length > 31) {
      this.alertService.showWarningAlert('Login Fehler', 'Der eingegeben Nutzername ist zu lang. Es sind maximal 31 Zeichen erlaubt');
      return;
    }

    const password = this.loginForm.controls.password.value;
    if (!password) {
      this.alertService.showWarningAlert('Login Fehler', 'Bitte geben sie ein Passwort ein');
      return;
    }

    this.authService.startSession(username, password).subscribe(
      res => {
        if (res instanceof HttpErrorResponse) {
          this.alertService.showWarningAlert('Login Fehler', 'Das angegebene Passwort ist falsch oder der Nutzer existiert nicht');
        } else {
          this.loginForm.reset();
          // if user was redirected from another page because of an invalid session return to that page after login
          this.router.navigateByUrl(this.returnUrl);
        }
      }
    );
  }
}
