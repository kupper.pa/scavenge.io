import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Hunt, Hunts } from '../../../../../backend/src/endpoints/responses';


@Component({
  selector: 'app-hunts-view',
  templateUrl: './hunts-view.component.html',
  styleUrls: ['./hunts-view.component.scss']
})
/**
 * Displays an overview of solvable and editable hunts
 */
export class HuntsViewComponent implements OnInit {
  received = new Array<Hunt>();
  created = new Array<Hunt>();

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.updateHuntsTable();
  }

  /**
   * Request lists of solvable and editable hunts form the backend
   */
  updateHuntsTable() {
    this.apiService.getReceivedHunts().subscribe((res: Hunts) => {
      this.received = res.hunts;
    });
    this.apiService.getCreatedHunts().subscribe((res: Hunts) => {
      this.created = res.hunts;
    });
  }
}
