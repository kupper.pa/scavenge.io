import { Component } from '@angular/core';

@Component({
  selector: 'app-contact-view',
  templateUrl: './contact-view.component.html',
  styleUrls: ['./contact-view.component.scss']
})
/**
 * Shows contact information for the creators of scavenge
 */
export class ContactViewComponent { }
