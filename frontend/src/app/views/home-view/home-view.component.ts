import { Component } from '@angular/core';

@Component({
  selector: 'app-home-view',
  templateUrl: './home-view.component.html',
  styleUrls: ['./home-view.component.scss']
})
/**
 * The landing page of the Application.
 * Contains a short Tutorial on how to use the Application
 */
export class HomeViewComponent { }
