import { Component, OnInit } from '@angular/core';
import { HuntDataService } from 'src/app/services/hunt-data.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-solve-view',
  templateUrl: './solve-view.component.html',
  styleUrls: ['./solve-view.component.scss']
})
export class SolveViewComponent implements OnInit {

  constructor(public dataService: HuntDataService, public route: ActivatedRoute) { }

  riddleID: number;
  solution: string;

  text = 'Rätsel wird gelöst...';

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params: ParamMap) => {
        this.riddleID = +params.get('id');
        this.solution = params.get('solution');

        this.dataService.solveRiddle(this.riddleID, { solution: this.solution }).subscribe(
          res => {
            if (res instanceof HttpErrorResponse) {
              this.text = 'Rätsel konnte nicht gelöst werden. Falsche Lösung?';
            } else {
              this.text = 'Rätsel wurde erfolgreich gelöst!';
            }
          }
        );
      }
    );
  }

}
