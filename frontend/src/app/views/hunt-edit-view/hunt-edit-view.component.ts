import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Hunt } from '../../../../../backend/src/endpoints/responses';
import { HuntDataService } from 'src/app/services/hunt-data.service';
import { ModalController } from '@ionic/angular';
import { OverlayEventDetail } from '@ionic/core';
import { RiddleEditModalComponent } from 'src/app/components/riddle-edit-modal/riddle-edit-modal.component';
import { HuntEditModalComponent } from 'src/app/components/hunt-edit-modal/hunt-edit-modal.component';
import { AlertService } from 'src/app/services/alert.service';
import { FormGroup, FormControl } from '@angular/forms';
import { validateUsername, validateEmail } from 'src/app/util/input-validation';
import { User } from '../../../../../backend/src/endpoints/requests';
import { HuntGraphComponent } from 'src/app/components/hunt-graph/hunt-graph.component';
import { RiddleEditListComponent } from 'src/app/components/riddle-edit-list/riddle-edit-list.component';


@Component({
  selector: 'app-hunt-edit-view',
  templateUrl: './hunt-edit-view.component.html',
  styleUrls: ['./hunt-edit-view.component.scss']
})
export class HuntEditViewComponent implements OnInit {
  huntID: number;
  hunt = { riddles: [], receiver: {}, editors: [] } as Hunt;

  addEditorForm = new FormGroup({
    editor: new FormControl(),
  });

  @ViewChild(HuntGraphComponent) graph: HuntGraphComponent;
  @ViewChild(RiddleEditListComponent) editList: RiddleEditListComponent;

  constructor(public dataService: HuntDataService, private route: ActivatedRoute, private router: Router, public alertService: AlertService, public modalController: ModalController) { }

  updateData(): void {
    this.dataService.getHunt(this.huntID).subscribe(
      hunt => {
        this.hunt = hunt;
      }
    );
  }

  onRiddleDataChanged() {
    this.updateData();
    this.editList.updatePredecessors();
    this.graph.updateData();
  }

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params: ParamMap) => {
        this.huntID = +params.get('id');
        this.updateData();
      }
    );
  }

  async removeHunt() {
    const confirmation = await this.alertService.showConfirmationAlert('Schnitzeljagd löschen', 'Wollen sie die Schnitzeljagd wirklich löschen?', 'Löschen');
    if (confirmation) {
      // is executed if user confirms deletion
      this.dataService.removeHunt(this.huntID).subscribe(
        res => {
          if (res instanceof HttpErrorResponse) {
            // deletion failed
            console.log(res.error);
          } else {
            // deletion successful
            this.router.navigate(['/hunts']);
          }
        }
      );
    }
  }

  async editTitleAndDescription() {
    // create confirmation dialog
    const modal = await this.modalController.create({
      component: HuntEditModalComponent,
      componentProps: {
        title: this.hunt.title,
        description: this.hunt.description,
        reward: this.hunt.reward,
      },
      cssClass: 'hunt-edit-modal',
    });

    modal.onDidDismiss().then((eventDetail: OverlayEventDetail) => {
      const data = eventDetail.data;
      if (!data) { return; }
      // is executed if user confirms changes
      this.dataService.updateHunt(this.huntID, { description: data.description, title: data.title }).subscribe(
        res => {
          if (res instanceof HttpErrorResponse) {
            // update failed
            this.onUpdateHuntFailed();
          } else {
            // updated successful
            this.updateData();
          }
        }
      );
    });
    modal.present();
  }

  async onUpdateHuntFailed() {
    this.alertService.showWarningAlert('Fehler beim Bearbeiten', 'Die Schnitzeljagd konnte aus einem unbekannten Grund nicht bearbeitet werden. Versuchen sie es bitte erneut.');
  }

  async createRiddle() {
    // create riddle dialog
    const modal = await this.modalController.create({
      component: RiddleEditModalComponent,
      componentProps: {
        title: '',
        description: '',
        solution: '',
      },
      cssClass: 'riddle-edit-modal',
    });

    modal.onDidDismiss().then((eventDetail: OverlayEventDetail) => {
      const data = eventDetail.data;
      if (!data) return;
      // is executed if user confirm riddle creation
      this.dataService.addRiddle(this.huntID, data).subscribe(
        res => {
          if (res instanceof HttpErrorResponse) {
            console.log(res.error);
          } else {
            this.updateData();
            this.editList.updatePredecessors();
            this.graph.updateData();
          }
        }
      );
    });
    modal.present();
  }

  onDeleteEditorPressed(user: User) {
    this.dataService.removeHuntEditor(this.huntID, user).subscribe (
      res => {
        if (res instanceof HttpErrorResponse) {
          this.alertService.showWarningAlert('Bearbeiter entfernen', 'Ein unbekannter Fehler ist aufgetreten. Versuchen sie es erneut');
        } else {
          this.updateData();
        }
      }
    );
  }

  onAddEditorFormSubmit() {
    const editor = this.addEditorForm.controls.editor.value;
    if (!editor || !(validateUsername(editor) || validateEmail(editor))) {
      this.alertService.showWarningAlert('Bearbeiter hinzufügen', 'Bitte geben sie einen gültigen Nutzernamen oder eine gültige Email-Adresse ein.');
      return;
    }
    this.dataService.addHuntEditor(this.huntID, { q: editor }).subscribe(
      res => {
        if (res instanceof HttpErrorResponse) {
          this.alertService.showWarningAlert('Bearbeiter hinzufügen', 'Der angegebene Nutzer existiert nicht');
        } else {
          this.updateData();
        }
      }
    );
  }
}
