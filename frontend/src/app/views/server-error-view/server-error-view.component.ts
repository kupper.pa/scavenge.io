import { Component } from '@angular/core';

@Component({
  selector: 'app-server-error-view',
  templateUrl: './server-error-view.component.html',
  styleUrls: ['./server-error-view.component.scss']
})
/**
 * Is shown when a HTTP Request is answered with a server error
 */
export class ServerErrorViewComponent { }
