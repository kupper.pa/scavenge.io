import { Component } from '@angular/core';

@Component({
  selector: 'app-page-not-found-view',
  templateUrl: './page-not-found-view.component.html',
  styleUrls: ['./page-not-found-view.component.scss']
})
/**
 * Is displayed when the user tries to acces an invalid route
 */
export class PageNotFoundViewComponent { }
