import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Me } from '../../../../../backend/src/endpoints/responses';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.scss']
})
/**
 * Displays information about the current user and shows logout and account deletion controls
 */
export class ProfileViewComponent implements OnInit {
  user: Me;

  constructor(public authService: AuthService, public apiService: ApiService, private router: Router, public alertService: AlertService) { }

  ngOnInit() {
    this.apiService.requestMeInfo().subscribe((res: Me | HttpErrorResponse) => res instanceof HttpErrorResponse ? console.error(res.error) : this.user = res);
  }

  /**
   * Is called when the logout button is pressed and ends the current session using the authentication service
   */
  onLogoutButtonPressed() {
    this.authService.endSession();
  }

  /**
   * Is calles then the delete account butten is pressed. Displays a confirmation dialog, which asks for the users password, and deletes the account when the password is valid
   */
  async onDeleteAccountButtonPressed() {
    const password = await this.alertService.showPasswordConfirmationAlert('Account löschen', 'Wollen sie die Ihren Account <strong>wirklich</strong> löschen? Diese Aktion kann nicht rückgängig gemacht werden. Bitte geben Sie zur Bestätigung ihr Passwort ein:', 'Löschen');
    if (password !== null) {
      // is executed if user confirms account deletion
      this.apiService.removeUser({ password }).subscribe(
        res => {
          if (res instanceof HttpErrorResponse) {
            // deletion failed
            this.alertService.showWarningAlert('Fehler beim Löschen', 'Der Account konnte nicht gelöscht werden. War das eingegebene Passwort ungültig?');
          } else {
            // deletion successful
            this.authService.endSession();
            this.router.navigate(['/']);
          }
        }
      );
    }
  }

  /**
   * Is calles then the delete account butten is pressed. Displays a confirmation dialog, which asks for the users password, and deletes the account when the password is valid
   */
  async onChangePasswordButtonPressed() {
    const result = await this.alertService.showPasswordChangeAlert();
    if (result !== null) {
      // is executed if user confirms password change
      if (result.password.length < 8 || !result.password.match(/.*[1-9].*/) || !result.password.match(/.*[A-ZÄÖÜ].*/) || !result.password.match(/.*[a-zäöüß].*/)) {
        this.alertService.showWarningAlert('Fehler beim Ändern des Passworts', 'Das Passwort muss mindestens 8 Zeichen lang sein und mindestens einen Großbuchstaben, einen Kleinbuchstaben und eine Zahl enthalten');
        return;
      }
      if (result.password !== result.passwordRepeat) {
        this.alertService.showWarningAlert('Fehler beim Ändern des Passworts', 'Die neuen Passwörter stimmen nicht überein.');
        return;
      }
      this.apiService.changePassword({ oldPassword: result.oldPassword, newPassword: result.password }).subscribe(
        res => {
          if (res instanceof HttpErrorResponse) {
            // password change failed
            this.alertService.showWarningAlert('Fehler beim Ändern des Passworts', 'Das Passwort konnte nicht geändert werden. War das eingegebene Passwort ungültig?');
          } else {
            // password changed successfully
            this.alertService.showWarningAlert('Passwortänderung', 'Das Passwort wurde erfolgreich geändert!');
          }
        }
      );
    }
  }
}
