import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import {
  CalendarModal,
  CalendarModalOptions,
} from 'ion2-calendar';
import { formatDate } from '@angular/common';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-hunt-create-view',
  templateUrl: './hunt-create-view.component.html',
  styleUrls: ['./hunt-create-view.component.scss']
})
/**
 * Provides an interface to create a new hunt
 */
export class HuntCreateViewComponent {

  addHuntForm = new FormGroup({
    title: new FormControl(),
    description: new FormControl(),
    reward: new FormControl(),
    receiver: new FormControl(),
    startDateReadable: new FormControl(),
    startDateRaw: new FormControl(),
  });

  constructor(public apiService: ApiService, private router: Router, public modalCtrl: ModalController, public alertService: AlertService) { }

  /**
   * Displays a date picker to enter the start date of the hunt
   */
  async openDatePicker() {
    const options: CalendarModalOptions = {
      title: 'Startdatum auswählen',
      closeLabel: 'Abbrechen',
      doneLabel: 'Fertig',
      defaultDate: new Date(),
      color: 'dark',
      weekdays: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
      weekStart: 1
    };

    const myCalendar = await this.modalCtrl.create({
      component: CalendarModal,
      componentProps: { options }
    });

    myCalendar.present();

    const event: any = await myCalendar.onDidDismiss();
    const date: Date = event.data.dateObj;
    date.setHours(0, 0, 0, 0);
    this.addHuntForm.controls.startDateReadable.setValue(formatDate(date, 'd. MMMM y', 'de'));
    this.addHuntForm.controls.startDateRaw.setValue(date);
  }

  /**
   * Is called when the hunt create form ist submitted. Checks the input for validity and creates a new hunt if successful
   */
  addHuntFormSubmit() {
    const controls = this.addHuntForm.controls;

    const title: string = controls.title.value;
    if (!title) {
      this.alertService.showWarningAlert('Schnitzeljagd erstellen', 'Bitte füge einen Titel hinzu');
      return;
    }

    const description: string = controls.description.value;
    if (!description) {
      this.alertService.showWarningAlert('Schnitzeljagd erstellen', 'Bitte füge eine Beschreibung hinzu');
      return;
    }

    const reward: string = controls.reward.value;
    if (!reward) {
      this.alertService.showWarningAlert('Schnitzeljagd erstellen', 'Bitte füge eine Belohnung hinzu');
      return;
    }

    const receiver: string = controls.receiver.value;
    if (!receiver) {
      this.alertService.showWarningAlert('Schnitzeljagd erstellen', 'Bitte füge einen Empfänger hinzu');
      return;
    }

    const date: string = controls.startDateRaw.value;
    if (!date) {
      this.alertService.showWarningAlert('Schnitzeljagd erstellen', 'Feld "Start Datum" ist ungültig');
      return;
    }

    this.apiService.addHunt({ title, description, start: new Date(date), receiver, reward }).subscribe(
      res => {
        if (res instanceof HttpErrorResponse) {
          if (res.status === 404) {
            this.alertService.showWarningAlert('Schnitzeljagd erstellen', 'Der im Feld "Empänger" eigetragene Nutzer existiert nicht');
          } else {
            this.alertService.showWarningAlert('Schnitzeljagd erstellen', 'Sie können keine Schnitzeljagd für sich selbst erstellen');
          }
        } else {
          this.router.navigate(['/hunt/edit/' + res.id]);
        }
      }
    );
  }
}
