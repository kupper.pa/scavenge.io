import { Component } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthService } from 'src/app/services/auth.service';
import { validateEmail, validatePassword } from 'src/app/util/input-validation';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-register-view',
  templateUrl: './register-view.component.html',
  styleUrls: ['./register-view.component.scss']
})
/**
 * Provides an interface for creating a new user
 */
export class RegisterViewComponent {
  registerForm = new FormGroup({
    username: new FormControl(),
    email: new FormControl(),
    password: new FormControl(),
    passwordConfirm: new FormControl()
  });

  constructor(public apiService: ApiService, private router: Router, public authService: AuthService, public alertService: AlertService) { }

  /**
   * is called when the register form is submitted. Validates the given information an registers a user using the api service
   */
  onRegisterFormSubmit() {
    const controls = this.registerForm.controls;

    // input validation
    const username = controls.username.value;
    if (!username) {
      this.alertService.showWarningAlert('Fehler beim Registrieren', 'Bitte geben sie einen Nutzernamen ein');
      return;
    }
    if (!username.match(/^[0-9A-Za-zAÖÜäöüß\._]+$/)) {
      this.alertService.showWarningAlert('Fehler beim Registrieren', 'Der eingegebene Nutzername enthält unzulässige Zeichen. Erlaubt sind Buchstaben, Ziffern und die Zeichen . und _');
      return;
    }

    let email: string = controls.email.value;
    if (!email) {
      this.alertService.showWarningAlert('Fehler beim Registrieren', 'Bitte geben sie eine Email Adresse ein');
      return;
    }

    // normalize email addres to comply with RFC 5322
    email = email.toLowerCase();
    if (!email || !validateEmail(email)) {
      this.alertService.showWarningAlert('Fehler beim Registrieren', 'Die eingegebene Email Adresse ist ungültig');
      return;
    }

    const password = controls.password.value;
    if (!password || !validatePassword(password)) {
      this.alertService.showWarningAlert('Fehler beim Registrieren', 'Das Passwort muss mindestens 8 Zeichen lang sein und mindestens einen Großbuchstaben, einen Kleinbuchstaben eine Zahl enthalten');
      return;
    }

    const passwordConfirm = controls.passwordConfirm.value;
    if (!passwordConfirm || passwordConfirm !== password) {
      this.alertService.showWarningAlert('Fehler beim Registrieren', 'Die eingegebenen Passwörter stimmen nicht überein');
      return;
    }

    this.apiService.registerUser({ username, email, password }).subscribe(
      res => {
        if (res instanceof HttpErrorResponse) {
          this.alertService.showWarningAlert('Fehler beim Registrieren', 'Fehler bei der Registrierung: ' + res.error.message);
        } else {
          this.authService.startSession(username, password).subscribe(
            result => {
              if (result instanceof HttpErrorResponse) {
                this.alertService.showWarningAlert('Fehler beim Registrieren', 'Registrierung erfolgreich. Fehler beim Anmelden' + result.error.message);
              } else {
                this.router.navigate(['/hunts']);
              }
            }
          );
        }
      }
    );
  }
}
