import { NextFunction, Request, Response } from 'express';
import * as config from '../config';
import { sleep } from '../utils/utils';

/**
 * Express middleware that adds a delay to the request for testing purposes
 */
export let delay = async (_: Request, __: Response, next: NextFunction) => (config.delay && await sleep(config.delay), next());
