import { NextFunction, Request, Response } from 'express';
import { toError } from '../error';

/**
 * An express error handler which sends errors occured in routes
 */
export let errorHandler = (err: any, _: Request, res: Response, __: NextFunction) => (
  console.error(`${err}`),
  toError(err).send(res)
);
