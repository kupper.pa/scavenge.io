import { NextFunction } from 'connect';
import { Request, Response } from 'express';
import { Request as Oauth2Request, Response as Oauth2Response } from 'oauth2-server';
import * as OAuth2Server from 'oauth2-server';
import { createClient } from 'redis';
import * as config from '../config';
import { Error } from '../error';
import { Model } from '../oauth/model';
import { loadUser } from '../utils/db';

/**
 * Save OAuth server and redis client instance to express request to be able to use them later
 */
export let attachOauthRedis = async () => {
  const redisClient = createClient(config.redis)
    .on('error', err => console.error(err.message));
  const oauth = new OAuth2Server({
    model: new Model(redisClient),
    accessTokenLifetime: config.oauth.accessTokenLifetime,
    refreshTokenLifetime: config.oauth.refreshTokenLifetime,
  });
  return (req: Request, _: Response, next: NextFunction) =>
    ([req.redis, req.oauth] = [redisClient, oauth], next());
};

/**
 * Authenticate request through OAuth server and save user object to request for use in endpoints
 *
 * @param req Express request
 * @param res Express response
 * @param next Express NextFunction
 */
export let authenticate = async (req: Request, res: Response, next: NextFunction) => {
  try {
    req.user = await loadUser((await req.oauth.authenticate(new Oauth2Request(req), new Oauth2Response(res))).user.id);
    next();
  } catch (e) {
    (new Error(e.code || 500, e.message)).send(res);
  }
};
