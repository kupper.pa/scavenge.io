import { Request } from 'express';
import { getConnection } from 'typeorm';
import { Hint } from '../entities/Hint';
import { Hunt } from '../entities/Hunt';
import { Riddle } from '../entities/Riddle';
import { User } from '../entities/User';
import { Error } from '../error';
import { wrapMiddelware as wrap } from '../utils/wrapper';

/**
 * Checks if the user is an editor of the hunt
 *
 * @param userId The user id
 * @param huntId The hunt id
 * @returns ```true``` iff the user is an editor of the hunt
 */
const isEditor = async (userId: number, huntId: number): Promise<boolean> => {
    return Boolean(await getConnection().getRepository(User)
        .createQueryBuilder('User')
        .leftJoinAndSelect('User.created', 'hunt')
        .where('User.id = :uid', { uid: userId })
        .andWhere('hunt.id = :hid', { hid: huntId })
        .getOne());
};

/**
 * Checks if the user is a receiver of the hunt
 *
 * @param userId The user id
 * @param huntId The hunt id
 * @returns ```true``` iff the user is the receiver of the hunt
 */
const isReceiver = async (userId: number, huntId: number): Promise<boolean> => {
    const hunt = await getConnection().getRepository(Hunt).findOne({ where: { id: huntId, receiver: userId } });
    return Boolean(hunt && hunt.start < new Date());
};

/**
 * Middleware that checks if a user is an editor of a hunt in ```/hunt/:id``` routes
 */
export let huntEditor = wrap('hunt editor auth', async (req: Request) => {
    if (!await isEditor(req.user.id, req.params.id)) throw new Error(403, 'you are not the editor of this hunt');
});

/**
 * Middleware that checks if a user is an editor or the receiver of the hunt in ```/hunt/:id``` routes.
 * A boolean whether the user is editor is stored in the attribute ```editor``` of the request.
 */
export let huntReceiverEditor = wrap('hunt receiver or editor auth', async (req: Request) => {
    const [receiver, editor] = await Promise.all([isReceiver(req.user.id, req.params.id), isEditor(req.user.id, req.params.id)]);
    if (!receiver && !editor) throw new Error(400, 'unauthorized');
    req.editor = editor;
});

/**
 * Middleware that checks if a user is an editor of the hunt the riddle in ```/riddle/:id``` routes belongs to
 */
export let riddleEditor = wrap('riddle editor auth', async (req: Request) => {
    const riddle = await getConnection().getRepository(Riddle).findOne({ where: { id: req.params.id }, relations: ['hunt'] });
    if (!riddle) throw new Error(404, 'riddle not found');
    if (!await isEditor(req.user.id, riddle.hunt.id)) throw new Error(403, 'you are not the editor of this hunt');
});

/**
 * Middleware that checks if a user is an editor or the hunt the riddle in ```/riddle/:id``` routes belongs to.
 * A boolean whether the user is editor is stored in the attribute ```editor``` of the request.
 */
export let riddleReceiverEditor = wrap('riddle receiver or editor auth', async (req: Request) => {
    const riddle = await getConnection().getRepository(Riddle).findOne({ where: { id: req.params.id }, relations: ['hunt'] });
    if (!riddle) throw new Error(404, 'riddle not found');
    const [receiver, creator] = await Promise.all([isReceiver(req.user.id, riddle.hunt.id), isEditor(req.user.id, riddle.hunt.id)]);
    if (!receiver && !creator) throw new Error(400, 'unauthorized');
    req.editor = creator;
});

/**
 * Middleware that checks if a user is an editor of the hunt the hint in ```/hint/:id``` routes belongs to
 */
export let hintEditor = wrap('hint editor auth', async (req: Request) => {
    const hint = await getConnection().getRepository(Hint).findOne({ where: { id: req.params.id }, relations: ['riddle', 'riddle.hunt'] });
    if (!hint) throw new Error(404, 'riddle not found');
    if (!await isEditor(req.user.id, hint.riddle.hunt.id)) throw new Error(403, 'you are not the editor of this hunt');
});

/**
 * Middleware that checks if a user is an editor or the hunt the hint in ```/hint/:id``` routes belongs to.
 * A boolean whether the user is editor is stored in the attribute ```editor``` of the request.
 */
export let hintReceiverEditor = wrap('hint receiver or editor auth', async (req: Request) => {
    const hint = await getConnection().getRepository(Hint).findOne({ where: { id: req.params.id }, relations: ['riddle', 'riddle.hunt'] });
    if (!hint) throw new Error(404, 'riddle not found');
    const [receiver, creator] = await Promise.all([isReceiver(req.user.id, hint.riddle.hunt.id), isEditor(req.user.id, hint.riddle.hunt.id)]);
    if (!receiver && !creator) throw new Error(400, 'unauthorized');
    req.editor = creator;
});
