import { Response } from 'express';

/**
 * InternalError represents an error that should not be throwed by application logic
 * This error type should ONLY be used for tooling like in utils/wrapper.ts
 */
export class InternalError {
  constructor(private source: string, public inner: any) { }
  toString() {
    const error = this.inner instanceof Error ? this.inner : null;
    const [message, code] = error ? [error['message'], error['code']] : [this.inner, null];
    return `error '${message}'${code ? ` with status code '${code}' ` : ' '}occurred in '${this.source}'`;
  }
}

/**
 * All errors in the application should be of this type if they should be send to the frontend
 * Instanses of this class can be throwed in middlewares or also in endpoint handler
 */
export class Error {
  constructor(private code: number = 500, private message: string = 'Internal Server Error') { }
  send = (res: Response) => res.status(this.code).json(this);
}

/**
 * Converts anything in either an Error if it was an InternalError or in an empty Error if its not
 */
export let toError = (some: any): Error => some instanceof InternalError && some.inner.code && some.inner || new Error();
