import { insertTestData as insertData, typeorm } from './config';
import { server as Server } from './endpoints/server';
import { insertTestData } from './testdata/testdata';
import { connect } from './utils/db';

/**
 * Main entry point of the app
 * This function will first try to connect to the database configured in the json config
 * Make sure the database server is running
 */
const main = async () => {
    await connect(typeorm);
    if (insertData) await insertTestData();
    const server = await Server();
    server.listen(8080);
};

main().catch(e => (console.error(e), process.exit(1)));
