import { File as FileEntity } from '../entities/File';
import { Hint as HintEntity } from '../entities/Hint';
import { Hunt as HuntEntity } from '../entities/Hunt';
import { Riddle as RiddleEntity } from '../entities/Riddle';
import { User as UserEntity } from '../entities/User';

/**
 * Represents a user in a response
 */
export class User {
    id: number;
    username: string;
    joined: Date;
    lastLogin: Date;

    constructor(user: UserEntity) {
        this.id = user.id;
        this.username = user.name;
        this.joined = user.joined;
        this.lastLogin = user.lastLogin;
    }
}

/**
 * Represents a list of users in a response, i.e. when searching a user
 */
export class Users {
    users: User[];

    constructor(users: UserEntity[]) {
        this.users = (users || []).map(elem => new User(elem));
    }
}

/**
 * Represents the own user in a response used for the profile view.
 * This may contain sensitive information that should not be sent to other users.
 */
export class Me {
    id: number;
    username: string;
    joined: Date;
    lastLogin: Date;
    email: string;

    constructor(user: UserEntity) {
        this.id = user.id;
        this.username = user.name;
        this.joined = user.joined;
        this.lastLogin = user.lastLogin;
        this.email = user.email;
    }
}

/**
 * Represents a file in responses
 */
export class File {
    hash: string;
    name: string;

    constructor(file: FileEntity) {
        this.name = file.name;
        this.hash = file.hash;
    }
}

/**
 * Represents a riddle in responses.
 * Some attributes may not be available for the receiver.
 */
export class Riddle {
    id: number;
    title: string;
    description: string;
    solution?: string;
    hunt?: number;
    files: File[];
    hints?: Hint[];
    predecessors?: Predecessor[];
    possPredecessors?: Predecessor[];

    constructor(riddle: RiddleEntity, editor: boolean, possPredecessors?: Predecessors) {
        this.id = riddle.id;
        this.title = riddle.title;
        this.description = riddle.description;
        this.files = (riddle.files || []).map(elem => new File(elem));
        if (riddle.hints) this.hints = riddle.hints.map(e => new Hint(e, editor));
        if (editor) this.solution = riddle.solution;
        if (riddle.hunt) this.hunt = riddle.hunt.id;
        if (riddle.riddles) this.predecessors = new Predecessors(riddle.riddles).predecessors;
        if (editor && possPredecessors) this.possPredecessors = possPredecessors.predecessors;
    }
}

/**
 * Represents a hunt in a response.
 * Some attributes may not be available for the receiver.
 */
export class Hunt {
    id: number;
    title: string;
    description: string;
    start: Date;
    editors?: User[];
    receiver?: User;
    completed?: string;
    reward?: string;
    riddles: number[] | Riddle[];

    constructor(hunt: HuntEntity, editor: boolean, editors?: UserEntity[]) {
        this.id = hunt.id;
        this.title = hunt.title;
        this.description = hunt.description;
        this.start = hunt.start;
        if (hunt.completed) {
            this.completed = hunt.completed.toString();
            this.reward = hunt.reward;
        }
        if (hunt.receiver) this.receiver = new User(hunt.receiver);
        if (!editor) this.riddles = [];
        else {
            this.reward = hunt.reward;
            this.riddles = (hunt.riddles || []).map(elem => elem.id);
        }
        if (editors) this.editors = new Users(editors).users;
    }
}

/**
 * Represents a hint in a response
 */
export class Hint {
    id: number;
    title?: string;
    description?: string;
    displayed: Date;
    displayAt?: Date;
    displayInHours?: number;

    constructor(hint: HintEntity, editor: boolean) {
        this.id = hint.id;
        if (editor || hint.displayed) this.title = hint.title;
        if (editor || hint.displayed) this.description = hint.description;
        this.displayed = hint.displayed;
        hint.displayAt ? this.displayAt = hint.displayAt : this.displayInHours = hint.displayInHours;
    }
}

/**
 * Represents a list of riddles in a response
 */
export class Riddles {
    riddles: Riddle[];

    constructor(riddles: RiddleEntity[], editor: boolean) {
        this.riddles = (riddles || []).map(elem => new Riddle(elem, editor));
    }
}

/**
 * represents a list of hunts in a response
 */
export class Hunts {
    hunts: Hunt[];

    constructor(hunts: HuntEntity[], editor: boolean) {
        this.hunts = (hunts || []).map(elem => new Hunt(elem, editor, elem.createdBy));
    }
}

/**
 * Represents a list of files in a response
 */
export class Files {
    files: File[];

    constructor(data: FileEntity[]) {
        this.files = (data || []).map(elem => new File(elem));
    }
}

/**
 * Represents a list of hints in a response
 */
export class Hints {
    hints: Hint[];

    constructor(hints: HintEntity[], editor: boolean) {
        this.hints = (hints || []).map(elem => new Hint(elem, editor));
    }
}

/**
 * Represents a predecessor riddle in a response.
 * There is no need for all attributes of a riddle.
 */
export class Predecessor {
    id: number;
    title: string;

    constructor(riddle: RiddleEntity) {
        this.id = riddle.id;
        this.title = riddle.title;
    }
}

/**
 * Represents a list of predecessor riddles
 */
export class Predecessors {
    predecessors: Predecessor[];

    constructor(riddles: RiddleEntity[]) {
        this.predecessors = (riddles || []).map(elem => new Predecessor(elem));
    }
}

/**
 * Represents a set of riddles in a directed graph. Directions go from predecessor to successor.
 */
export class Graph {
    nodes: Array<{ id: string, label: string, description: string, solved: Date, unlocked: boolean }>;
    links: Array<{ source: string, target: string }>;

    constructor(nodes: RiddleEntity[]) {
        this.nodes = nodes.map(elem => ({
            id: elem.id.toString(),
            label: elem.title,
            description: elem.description,
            solved: elem.solved,
            unlocked: elem.riddles.reduce((flat: boolean, e) => (flat && e.solved) ? true : false, true)
        }));
        this.links = nodes.map(e => e.riddles.map(e2 => ({
            source: e2.id.toString(),
            target: e.id.toString()
        }))).reduce((flat, e) => flat.concat(e), []);
    }
}
