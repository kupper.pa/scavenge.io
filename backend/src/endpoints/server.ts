import * as bodyParser from 'body-parser';
import * as compression from 'compression';
import { static as serveStatic } from 'express';
import * as helmet from 'helmet';
import * as logger from 'morgan';
import { path } from '../config';
import { deleteFile, getFile, uploadFile } from '../handler/filehosting';
import { addHint, editHint, getHint, listHints, removeHint } from '../handler/hints';
import { addEditor, addHunt, editHunt, getActiveRiddles, getCompletedRiddles, getCreatedHunts, getGraph, getHunt, getReceivedHunts, getRiddlesFromHunt, removeEditor, removeHunt } from '../handler/hunts';
import { indexHtml, noRoute } from '../handler/misc';
import { obtainToken as obtainToken, revokeToken } from '../handler/oauth';
import { register } from '../handler/register';
import { addPredecessor, addRiddle, editRiddle, getPossiblePredecessors, getPredecessors, getRiddle, listFiles, removePredecessor, removeRiddle, setPredecessors, solveRiddle } from '../handler/riddles';
import { changePassword, deleteUser, getUser, me, searchUser } from '../handler/users';
import { hintEditor, hintReceiverEditor, huntEditor, huntReceiverEditor, riddleEditor, riddleReceiverEditor } from '../middlewares/authorize';
import { delay } from '../middlewares/delay';
import { errorHandler } from '../middlewares/error';
import { attachOauthRedis, authenticate } from '../middlewares/oauth';
import { all, builder as serverBuilder, del, get, post, put, scope } from '../utils/server';
import { bundle as use } from '../utils/utils';
import { AddHint, AddHunt, AddRiddle, EditHint, EditHunt, Editor, EditRiddle, Expand, Hash, IDArray, Password, PasswordChange, Redis, Register, RequestID, RevokeToken, Search, SearchBody, Solution, User } from './requests';

/**
 * Create express server with the given routes and middlewares.
 * This function can be used as enpoint documentation.
 */
export let server = async () =>
    serverBuilder(
        use(helmet(), logger('dev'), compression(), serveStatic(path.staticFiles)),
        scope(
            use(bodyParser.urlencoded({ extended: true }), bodyParser.json(), await attachOauthRedis(), delay),
            post('/api/user/register').handler(register).data(Register),
            all('/api/user/login').rawHandler(obtainToken),
            scope(
                use(authenticate),
                get('/api/me').handler(me).data(User),
                post('/api/user/logout').handler(revokeToken).data(RevokeToken, Redis),
                get('/api/user/search').handler(searchUser).data(Search),
                get('/api/user/:id').handler(getUser),
                put('/api/user/password').handler(changePassword).data(User, PasswordChange),
                del('/api/user/remove').handler(deleteUser).data(Password, User),
                get('/api/hunt/received').handler(getReceivedHunts).data(User),
                get('/api/hunt/created').handler(getCreatedHunts).data(User),
                post('/api/hunt').handler(addHunt).data(AddHunt, User),
                scope(
                    use(huntEditor),
                    put('/api/hunt/:id').handler(editHunt).data(EditHunt),
                    del('/api/hunt/:id').handler(removeHunt),
                    post('/api/hunt/:id/riddle').handler(addRiddle).data(AddRiddle),
                    post('/api/hunt/:id/editor').handler(addEditor).data(SearchBody, User),
                    del('/api/hunt/:id/editor').handler(removeEditor).data(RequestID, User),
                ),
                scope(
                    use(huntReceiverEditor),
                    get('/api/hunt/:id').handler(getHunt).data(User, Editor, Expand),
                    get('/api/hunt/:id/riddle').handler(getRiddlesFromHunt).data(Editor, Expand),
                    get('/api/hunt/:id/completed').handler(getCompletedRiddles).data(Expand),
                    get('/api/hunt/:id/active').handler(getActiveRiddles).data(Editor, Expand),
                    get('/api/hunt/:id/graph').handler(getGraph).data(Editor)
                ),
                scope(
                    use(riddleEditor),
                    put('/api/riddle/:id').handler(editRiddle).data(EditRiddle),
                    del('/api/riddle/:id').handler(removeRiddle),
                    post('/api/riddle/:id/predecessor').handler(addPredecessor).data(RequestID),
                    del('/api/riddle/:id/predecessor').handler(removePredecessor).data(RequestID),
                    put('/api/riddle/:id/predecessor').handler(setPredecessors).data(IDArray),
                    get('/api/riddle/:id/possiblepredecessor').handler(getPossiblePredecessors),
                    del('/api/riddle/:id/file/:hash').handler(deleteFile).data(Hash),
                    post('/api/riddle/:id/file').rawHandler(uploadFile),
                    post('/api/riddle/:id/hint').handler(addHint).data(AddHint),
                ),
                scope(
                    use(riddleReceiverEditor),
                    get('/api/riddle/:id').handler(getRiddle).data(Editor),
                    post('/api/riddle/:id/solve').handler(solveRiddle).data(Solution),
                    get('/api/riddle/:id/predecessor').handler(getPredecessors),
                    get('/api/riddle/:id/file').handler(listFiles),
                    get('/api/riddle/:id/hint').handler(listHints).data(Editor),
                    get('/api/riddle/:id/file/:hash').rawHandler(getFile),
                ),
                scope(
                    use(hintEditor),
                    del('/api/hint/:id').handler(removeHint),
                    put('/api/hint/:id').handler(editHint).data(EditHint),
                ),
                scope(
                    use(hintReceiverEditor),
                    get('/api/hint/:id').handler(getHint).data(Editor),
                ),
            ),
        ),
        all('/api/*').handler(noRoute),
        get('*').rawHandler(indexHtml),
    ).use(errorHandler);
