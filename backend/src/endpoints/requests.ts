import { RedisClient } from 'redis';
import { User as UserEntity } from '../entities/User';
import { notEmpty, optional, reqBody, reqParam, reqQuery, reqRaw, required } from '../utils/meta';

@reqRaw
export class User {
    @required
    user: UserEntity;
}

@reqRaw
export class Redis {
    @required
    redis: RedisClient;
}

@reqRaw
export class Editor {
    @optional
    editor: boolean;
}

@reqQuery
export class Expand {
    @optional
    expand: number;
}

@reqQuery
export class Search {
    @required
    q: string;
}

@reqBody
export class SearchBody {
    @required
    q: string;
}

@reqParam
export class ID {
    @required
    id: number;
}

@reqParam
export class Hash {
    @required
    hash: string;
}

@reqBody
export class RevokeToken {
    @optional
    accessToken: string;
    @optional
    refreshToken: string;
}

@reqBody
export class RequestID {
    @required
    id: number;
}

@reqBody
export class IDArray {
    @required
    ids: number[];
}

@reqBody
export class Register {
    @required
    username: string;
    @required
    email: string;
    @required
    password: string;
}

@reqBody
export class Password {
    @required
    password: string;
}

@reqBody
export class PasswordChange {
    @required
    oldPassword: string;
    @required
    newPassword: string;
}

@reqBody
export class AddHunt {
    @required
    title: string;
    @required
    description: string;
    @required
    reward: string;
    @required
    start: Date;
    @required
    receiver: string;
}

@notEmpty
@reqBody
export class EditHunt {
    @optional
    title?: string;
    @optional
    description?: string;
    @optional
    reward?: string;
    @optional
    start?: Date;
    @optional
    receiver?: number;
}

@reqBody
export class AddRiddle {
    @required
    title: string;
    @optional
    description?: string;
    @optional
    solution?: string;
}

@notEmpty
@reqBody
export class EditRiddle {
    @optional
    title?: string;
    @optional
    description?: string;
    @optional
    solution?: string;
}

@reqBody
export class AddHint {
    @required
    title: string;
    @required
    description: string;
    @optional
    displayAt?: Date;
    @optional
    displayInHours?: number;
}

@notEmpty
@reqBody
export class EditHint {
    @optional
    title?: string;
    @optional
    description?: string;
    @optional
    displayAt?: Date;
    @optional
    displayInHours?: number;
}

@reqBody
export class Solution {
    @required
    solution: string;
}
