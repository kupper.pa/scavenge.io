import { promises as fs } from 'fs';
import { join } from 'path';
import { getConnection } from 'typeorm';
import { path } from '../config';
import { File } from '../entities/File';
import { Hunt } from '../entities/Hunt';
import { Riddle } from '../entities/Riddle';
import { User } from '../entities/User';
import { hashPw } from '../utils/utils';

/**
 * Inserts test data into the database
 */
export async function insertTestData() {

    const con = getConnection();

    //// Users /////
    const userRealKaiserKatze: User = {
        id: 1,
        name: 'realKaiserKatze',
        email: 'tarek.ziesak@mailbox.org',
        password: hashPw('Test1234'),
        joined: new Date(2015, 10, 3, 20, 55),
        lastLogin: new Date(),
        hunts: [],
        created: []
    };

    const userPakuppa: User = {
        id: 2,
        name: 'pakuppa',
        email: 'kupper.pa@gmail.com',
        password: hashPw('Test1234'),
        joined: new Date(2016, 8, 20, 21, 23),
        lastLogin: new Date(),
        hunts: [],
        created: []
    };

    const userTomathomas: User = {
        id: 3,
        name: 'tomathomas',
        email: 'thomas.moeller32@gmail.com',
        password: hashPw('Test1234'),
        joined: new Date(2012, 11, 45, 5, 1),
        lastLogin: new Date(),
        hunts: [],
        created: []
    };

    const users = con.getRepository(User);

    await users.save(userRealKaiserKatze);
    await users.save(userPakuppa);
    await users.save(userTomathomas);


    ///// Hunts /////
    const huntArch: Hunt = {
        id: 1,
        title: 'I use Arch btw.',
        description: 'Eine epische Reise durch die Tiefen einer Linux Distribution,\ndie dich vor unüberwindbare Herausforderungen stellen wird.',
        start: new Date(),
        completed: null,
        receiver: userPakuppa,
        createdBy: [userRealKaiserKatze],
        reward: 'You use Arch now btw.',
        riddles: []
    };

    const huntPoem: Hunt = {
        id: 2,
        title: 'Reime sind voll toll',
        description: 'Egal wie dicht du bist: Ich bin Dichter. Und deshalb stellen dich diese unglaublich schweren Reime vor ungeahnte Schwierigkeiten',
        start: new Date(),
        completed: null,
        receiver: userRealKaiserKatze,
        createdBy: [userTomathomas],
        reward: 'Roses are red,\nViolets are blue.\nYou solved the riddle\nWhoohoo, whoohoo!',
        riddles: []
    };

    const huntMaths: Hunt = {
        id: 3,
        title: 'Das ultimative Mathequiz',
        description: 'Neulich meinen Mathelehrer angerufen.\n\nDamit hat er nicht gerechnet.',
        start: new Date(),
        completed: null,
        receiver: userTomathomas,
        createdBy: [userPakuppa],
        reward: 'The answer to life the universe and everything is: 42',
        riddles: []
    };

    const hunts = con.getRepository(Hunt);

    await hunts.save(huntArch);
    await hunts.save(huntPoem);
    await hunts.save(huntMaths);


    ///// Riddles /////
    const archJournyBegins: Riddle = {
        id: 1,
        title: 'Die Reise beginnt',
        description: 'Du stehst am Rande einer neuen Arch Installation. Wieviel Zeit nimmst du dir dafür? Tipp: Angabe in Wochen.',
        solved: null,
        solution: '3',
        hunt: huntArch,
        riddles: [],
        files: [],
        hints: []
    };

    const archUnsurmountable: Riddle = {
        id: 2,
        title: 'Die unüberwindbare Herausforderung',
        description: 'Du hast dich in den kräftigen Fängen eines grausamen Ungeheuers mit dem Namen \"Vim der unterminierbare\" verfangen. Glücklicherweise hast du ein Planck zur Hand. Wie befreist du dich aus den Fängen dieses Biests?',
        solved: null,
        solution: 'Esc + :wq + Enter',
        hunt: huntArch,
        riddles: [archJournyBegins],
        files: [],
        hints: []
    };

    const archSirenes: Riddle = {
        id: 3,
        title: 'Die Sirenen',
        description: 'Im Laufe deiner Reise wirst du verführt von dem unwiederstehlichen Gesang der Anhänger von Microschrott und angebissenen Äpfeln. \"Mein Letztes Update hat mir nicht das System zerschossen\" singen sie. Mit aller Kraft brüllst du ihnen entgegen:',
        solved: null,
        solution: 'I use Arch btw.',
        hunt: huntArch,
        riddles: [archUnsurmountable],
        files: [],
        hints: []
    };

    const poemForest: Riddle = {
        id: 4,
        title: 'Der Wald',
        description: 'Bäume, Bäume nichts als Bäume und dazwischen Zwischenbäume und davor, man glaubt es kaum. Noch ein ...',
        solved: null,
        solution: 'Baum',
        hunt: huntPoem,
        riddles: [],
        files: [],
        hints: []
    };

    const poemPanther: Riddle = {
        id: 5,
        title: 'Der Panther',
        description: 'Sein Blick ist vom Vorübergehn der Stäbe so müd geworden, daß er nichts mehr hält. Ihm ist, als ob es tausend Stäbe gäbe und hinter tausend Stäben keine Welt.  Der weiche Gang geschmeidig starker Schritte, der sich im allerkleinsten Kreise dreht, ist wie ein Tanz von Kraft um eine Mitte, in der betäubt ein großer Wille steht.  Nur manchmal schiebt der Vorhang der Pupille sich lautlos auf –. Dann geht ein Bild hinein, geht durch der Glieder angespannte Stille – und hört im Herzen auf zu ...',
        solved: null,
        solution: 'sein',
        hunt: huntPoem,
        riddles: [poemForest],
        files: [],
        hints: []
    };

    const poemOxymoron: Riddle = {
        id: 6,
        title: 'Extremster Oxymoronismus',
        description: 'Dunkel war’s, der Mond schien helle, schneebedeckt die grüne Flur, als ein Wagen blitzesschnelle, langsam um die Ecke fuhr.  Drinnen saßen stehend Leute, schweigend ins Gespräch vertieft, als ein totgeschoss’ner Hase auf der Sandbank Schlittschuh lief.  Und ein blondgelockter Jüngling mit kohlrabenschwarzem Haar saß auf einer grünen Kiste, die rot angestrichen war.  Neben ihm ’ne alte Schrulle, zählte kaum erst sechzehn Jahr, in der Hand ’ne Butterstulle, die mit Schmalz bestrichen ...',
        solved: null,
        solution: 'war',
        hunt: huntPoem,
        riddles: [poemPanther],
        files: [],
        hints: []
    };

    const mathEasy: Riddle = {
        id: 7,
        title: 'Easy does it',
        description: 'Was ist 1 + 1',
        solved: null,
        solution: 'Eine Matheaufgabe',
        hunt: huntMaths,
        riddles: [],
        files: [],
        hints: []
    };

    const mathSerious: Riddle = {
        id: 8,
        title: 'Getting serious',
        description: 'In einem zylinderförmigen Eimer mit einem Innendurchmesser von 16 cm befinden sich 1,6 Liter Wasser. In diesen Eimer wird nun ein ebenfalls zylinderförmiger Metallbecher mit einem Außendurchmesser von 8 cm, einer Außenhöhe von 10 cm und einer Wandstärke von 1 cm mit der Öffnung nach oben gestellt. Der Becher ist so schwer, dass er auf dem Boden des Eimers stehen bleibt. In den Metallbecher ist nun ein Teil des Wassers geflossen.  Berechnen Sie, wie hoch das Wasser in dem Becher steht!',
        solved: null,
        solution: '3,26 cm',
        hunt: huntMaths,
        riddles: [mathEasy],
        files: [],
        hints: []
    };

    const mathPNP: Riddle = {
        id: 9,
        title: 'Asking for a Friend, obviously',
        description: 'Beweise dass P=NP',
        solved: null,
        solution: '#isso',
        hunt: huntMaths,
        riddles: [mathSerious],
        files: [],
        hints: []
    };

    const riddles = con.getRepository(Riddle);

    await riddles.save(archJournyBegins);
    await riddles.save(archUnsurmountable);
    await riddles.save(archSirenes);

    await riddles.save(poemForest);
    await riddles.save(poemPanther);
    await riddles.save(poemOxymoron);

    await riddles.save(mathEasy);
    await riddles.save(mathSerious);
    await riddles.save(mathPNP);


    ///// Files /////

    await fs.copyFile(join(__dirname, '..', '..', 'src', 'testdata', 'files', '4bb04e9dc7b516942874fbb378fa525ca8d07ce85bda30d49b2ec72b4db959f4'), path.volumeMount + '/4bb04e9dc7b516942874fbb378fa525ca8d07ce85bda30d49b2ec72b4db959f4');
    await fs.copyFile(join(__dirname, '..', '..', 'src', 'testdata', 'files', 'a69feacd4133b29c6c46b7d6e994fc25fa38e38bfdb7b456c606cd10b33dbe7b'), path.volumeMount + '/a69feacd4133b29c6c46b7d6e994fc25fa38e38bfdb7b456c606cd10b33dbe7b');
    await fs.copyFile(join(__dirname, '..', '..', 'src', 'testdata', 'files', 'bf9a9a603af120164375b984603970b40e8be6d9517f025494eec8544fa66e27'), path.volumeMount + '/bf9a9a603af120164375b984603970b40e8be6d9517f025494eec8544fa66e27');
    await fs.copyFile(join(__dirname, '..', '..', 'src', 'testdata', 'files', 'e9330d223d933615fdc4df434d1925a0953c5a9216c9c3f078145cef613780cb'), path.volumeMount + '/e9330d223d933615fdc4df434d1925a0953c5a9216c9c3f078145cef613780cb');

    const filePanther: File = {
        hash: '4bb04e9dc7b516942874fbb378fa525ca8d07ce85bda30d49b2ec72b4db959f4',
        name: 'panther.jpg',
        mimetype: 'image/jpeg',
        riddle: poemPanther,
    };

    const filePNP: File = {
        hash: 'a69feacd4133b29c6c46b7d6e994fc25fa38e38bfdb7b456c606cd10b33dbe7b',
        name: 'pvsnp.pdf',
        mimetype: 'application/pdf',
        riddle: mathPNP,
    };

    const fileArch: File = {
        hash: 'bf9a9a603af120164375b984603970b40e8be6d9517f025494eec8544fa66e27',
        name: 'archlinux.jpg',
        mimetype: 'image/jpeg',
        riddle: archJournyBegins,
    };

    const fileGraph: File = {
        hash: 'e9330d223d933615fdc4df434d1925a0953c5a9216c9c3f078145cef613780cb',
        name: 'skizze.svg',
        mimetype: 'image/svg+xml',
        riddle: mathSerious,
    };

    const files = con.getRepository(File);

    await files.save(fileArch);
    await files.save(fileGraph);
    await files.save(filePNP);
    await files.save(filePanther);
}
