/**
 * Specifies the format of access or refresh tokens stored in redis server
 */
export class StoreToken {
    typ: 'a' | 'r';
    exp: Date;
    cli: string;
    usr: number;
}
