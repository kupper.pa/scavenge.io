import { compare } from 'bcryptjs';
import { Client, PasswordModel, RefreshToken, RefreshTokenModel, Token, User } from 'oauth2-server';
import { RedisClient } from 'redis';
import { getConnection } from 'typeorm';
import * as config from '../config';
import { User as UserEntity } from '../entities/User';
import { getTokenAsync } from '../utils/redis';
import { StoreToken } from './types';

/**
 * Implementation of the model for the oauth server using password and regresh grant and redis as storage backend.
 * The specification can be found here: https://oauth2-server.readthedocs.io/en/latest/model/spec.html
 */
export class Model implements RefreshTokenModel, PasswordModel {

    /**
     * Constructor for OAuth server model with redis instance
     *
     * @param redis A redis client instance
     */
    constructor(private redis: RedisClient) { }

    /**
     * Scopes are not used in this project, but the specification requires the function verifyScope. Therefore all scopes are allowed.
     */
    verifyScope = async () => true;

    /**
     * Loads an access token from redis server. See the specification for details.
     *
     * @param accessToken The access token which should be loaded
     */
    async getAccessToken(accessToken: string): Promise<Token> {
        const token = await getTokenAsync(this.redis, accessToken);
        if (!token || token.typ !== 'a') return null;
        return {
            client: token.cli as any,
            accessToken,
            accessTokenExpiresAt: new Date(token.exp),
            user: { id: token.usr }
        };
    }

    /**
     * Loads the OAuth client. There is only one client for the web app. See specification for details.
     */
    getClient = async () => config.oauth;

    /**
     * Load a user from the database. See specification for details.
     *
     * @param username The username of the user
     * @param password The password of the user
     */
    async getUser(username: string, password: string): Promise<User> {
        let user = await getConnection().getRepository(UserEntity).findOne({ name: username });
        if (!user) user = await getConnection().getRepository(UserEntity).findOne({ email: username.toLowerCase() });
        if (user && await compare(password, user.password)) {
            user.lastLogin = new Date();
            await getConnection().getRepository(UserEntity).save(user);
            return { id: user.id };
        }
        return null;
    }

    /**
     * Stores a new access token. See specification for details.
     *
     * @param token The token that should be stored
     * @param client The client being used
     * @param user The user the token belongs to
     */
    async saveToken(token: Token, client: Client, user: User): Promise<Token> {
        const saveToken = token;
        token.client = client;
        token.user = user;
        const accessToken: StoreToken = {
            typ: 'a',
            exp: saveToken.accessTokenExpiresAt,
            cli: client.id,
            usr: user.id
        };
        const refreshToken: StoreToken = {
            typ: 'r',
            exp: saveToken.refreshTokenExpiresAt,
            cli: saveToken.client.id,
            usr: saveToken.user.id
        };
        this.redis.set(token.accessToken, JSON.stringify(accessToken), 'EX', config.oauth.accessTokenLifetime);
        this.redis.set(token.refreshToken, JSON.stringify(refreshToken), 'EX', config.oauth.refreshTokenLifetime);
        return saveToken;
    }

    /**
     * Loads a refresh token from the redis server. See the specification for details.
     *
     * @param refreshToken The refresh token which should be loaded
     */
    async getRefreshToken(refreshToken: string): Promise<RefreshToken> {
        const token = await getTokenAsync(this.redis, refreshToken);
        if (!token || token.typ !== 'r') return null;
        return {
            client: { id: token.cli } as any,
            refreshToken,
            refreshTokenExpiresAt: new Date(token.exp),
            user: { id: token.usr }
        };
    }

    /**
     * Deletes a token from the redis server. See specification for details.
     *
     * @param token The token that should be revoked
     */
    async revokeToken(token: Token) {
        if (token.accessToken) this.redis.del(token.accessToken);
        if (token.refreshToken) this.redis.del(token.refreshToken);
        return true;
    }
}
