/**
 * The type of a node. The id of a riddle can be used.
 */
export type Node = number;

/**
 * The type of an edge between two nodes
 */
export type Edge = [Node, Node];

/**
 * Implementation of a directed graph that can check whether there's a path between two nodes or not
 */
export class Graph {
    adjList: Map<number, number[]>;

    /**
     * The constructor for the graph taking all nodes and edges. There's no need to modify them later.
     *
     * @param nodes All nodes of the graph
     * @param edges All edges of the graph
     */
    constructor(nodes: Node[], edges: Edge[]) {
        this.adjList = new Map();
        for (const n of nodes) this.adjList.set(n, []);
        for (const e of edges) this.adjList.get(e[0]).push(e[1]);
    }

    /**
     * Checks if there's a path between source and destination
     *
     * @param src The source node
     * @param dest The destination node
     */
    isPath(src: Node, dest: Node) {
        const visited = new Map<number, boolean>();
        let current = [src];
        while (current.length) {
            for (const node of current) {
                visited.set(node, true);
                if (node === dest) return true;
            }
            current = current.reduce((flat, elem) => flat.concat(this.adjList.get(elem)), [])
                .filter((node, i, arr) => arr.indexOf(node) === i && !visited.get(node));
        }
        return false;
    }
}
