import { getConnection } from 'typeorm';
import { AddHunt, EditHunt, RequestID, Search } from '../endpoints/requests';
import * as response from '../endpoints/responses';
import { Hunt } from '../entities/Hunt';
import { Riddle } from '../entities/Riddle';
import { User } from '../entities/User';
import { Error } from '../error';
import { allDepsSolved, deleteHunt, loadHunt, loadUser, loadUserByNameOrEmail } from '../utils/db';

/**
 * Auxiliary function
 *
 * @param cond
 * @param data
 * @returns [[data]] if [[cond]] is true, otherwise an empty array
 */
const expandIf = <T>(cond: boolean, ...data: T[]) => cond ? data : [];

/**
 * Handle ```POST /hunt``` endpoint,
 * scope: authenticated
 *
 * @param data The information for the new hunt
 * @param user The user currently logged in
 */
export let addHunt = async (data: AddHunt, user: User) => {
    const hunt = new Hunt();
    hunt.title = data.title;
    hunt.description = data.description;
    hunt.reward = data.reward;
    hunt.start = data.start;
    if (data.receiver === user.name)
        throw new Error(400, 'You cannot create a hunt for yourself');
    if (data.receiver)
        hunt.receiver = await loadUserByNameOrEmail(data.receiver, new Error(404, 'receiver does not exist'));
    await getConnection().getRepository(Hunt).insert(hunt);
    await getConnection().createQueryBuilder().insert().into('user_created__hunt').values({ userId: user.id, huntId: hunt.id }).execute();
    return new response.Hunt(hunt, true);
};

/**
 * Handle ```DELETE /hunt/<id>``` endpoint,
 * scope: editor
 *
 * @param id The hunt id
 */
export let removeHunt = async (id: number) => {
    const hunt = await getConnection().getRepository(Hunt).findOne({ where: { id }, relations: ['receiver', 'riddles', 'riddles.files'] });
    if (!hunt) throw new Error(400, 'hunt not found');
    await deleteHunt(hunt);
};

/**
 * Handle ```PUT /hunt/<id>``` endpoint,
 * scope: editor
 *
 * @param id The hunt id
 * @param data The information beeing updated
 */
export let editHunt = async (id: number, data: EditHunt) => {
    let hunt = await loadHunt(id);
    hunt = { ...hunt, ...{ ...data, receiver: undefined } };
    if (data.receiver)
        hunt.receiver = await loadUser(data.receiver, new Error(400, 'receiver does not exist'));
    if (!await getConnection().getRepository(Hunt).save(hunt)) throw new Error();
    return new response.Hunt(hunt, true);
};

/**
 * Handle ```GET /hunt/<id>``` endpoint,
 * scope: editor, receiver
 *
 * @param id The hunt id
 * @param editor A boolean indicating whether the user is editor
 */
export let getHunt = async (id: number, user: User, editor: boolean, expand?: number) => {
    const hunt = await getConnection().getRepository(Hunt).findOne({
        where: { id },
        relations: [
            'receiver', 'riddles', 'createdBy',
            ...expandIf(expand > 0, 'riddles.files', 'riddles.hints')
        ]
    });
    return new response.Hunt(hunt, editor, hunt.createdBy.filter(elem => elem.id !== user.id));
};

/**
 * Handle ```GET /hunt/<id>/riddle``` endpoint,
 * scope: editor, receiver
 *
 * @param id The hunt id
 * @param expand If [[expand]] is at least 1, complete files and hints will be in the riddles in the response.
 */
export let getRiddlesFromHunt = async (id: number, editor: boolean, expand?: number) => {
    const hunt = await getConnection().getRepository(Hunt).findOne({
        where: { id },
        relations: [...['riddles'], ...expandIf(expand > 0, 'riddles.files', 'riddles.hints')],
    });
    if (!hunt) throw new Error(404, 'hunt not found');
    if (!editor && !hunt.completed) throw new Error(403, 'unauthorized');
    return new response.Riddles(hunt.riddles, editor);
};

/**
 * Handle ```/hunt/received``` endpoint,
 * scope: authenticated
 *
 * @param user The user currently logged in
 * @returns All hunts the user received
 */
export let getReceivedHunts = async (user: User) => {
    const hunts = await getConnection().getRepository(Hunt)
        .createQueryBuilder('Hunt')
        .leftJoinAndSelect('Hunt.riddles', 'riddles')
        .where('Hunt.receiver = :rid', { rid: user.id })
        .andWhere('Hunt.start <= :start', { start: new Date() })
        .leftJoinAndSelect('Hunt.createdBy', 'createdBy')
        .getMany();
    return new response.Hunts(hunts, false);
};

/**
 * Handle ```/hunt/created``` endpoint,
 * scope: authenticated
 *
 * @param user The user currently logged in
 * @returns All hunts the user created
 */
export let getCreatedHunts = async (user: User) => {
    const me = await getConnection().getRepository(User).findOne({
        where: { id: user.id },
        relations: [
            'created', 'created.riddles', 'created.receiver',
            // ...expand_if(expand > 1, 'created.riddles.files', 'created.riddles.hints')
        ],
    });
    return new response.Hunts(me.created, true);
};

/**
 * Handle ```POST /hunt/:id/editor``` endpoint,
 * scope: editor
 *
 * @param id The hunt id
 * @param data The complete username of the user that should be added as editor
 * @param user The use currently logged in
 */
export let addEditor = async (id: number, data: Search, user: User) => {
    const hunt = await loadHunt(id);
    if (hunt.receiver.name === data.q) throw new Error(400, 'cannot add receiver as editor');
    const newUser = await loadUserByNameOrEmail(data.q);
    if (!newUser) throw new Error();
    if (newUser.id === user.id) throw new Error(400, 'user is already editor');
    await getConnection().createQueryBuilder().insert().into('user_created__hunt').values({ userId: newUser.id, huntId: id }).execute();
};

/**
 * Handle ```DELETE /hunt/:id/editor``` endpoint,
 * scope: editor
 *
 * @param id The hunt id
 * @param data The complete username of the user that should be deleted from editors
 * @param user The use currently logged in
 */
export let removeEditor = async (id: number, data: RequestID, user: User) => {
    await loadHunt(id);
    const oldUser = await getConnection().getRepository(User).findOne({ id: data.id });
    if (!oldUser) throw new Error();
    if (oldUser.id === user.id) throw new Error(400, 'not possible to remove yourself');
    await getConnection().createQueryBuilder().delete().from('user_created__hunt')
        .where({userId: oldUser.id, huntId: id}).execute();
};


/**
 * Handle ```/hunt/<id>/completed``` endpoint,
 * scope: editor, receiver
 *
 * @param id The hunt id
 */
export let getCompletedRiddles = async (id: number, expand: number) => {
    const hunt = await loadHunt(id, 'riddles', 'riddles.riddles', ...expandIf(expand > 0, 'riddles.files', 'riddles.hints'));
    const arr: response.Riddle[] = [];
    hunt.riddles.forEach(element => {
        if (allDepsSolved(element) && element.solved) arr.push(new response.Riddle(element, true));
    });
    return arr;
};

/**
 * Handle ```/hunt/<id>/active``` endpoint,
 * scope: editor, receiver
 *
 * @param id The hunt id
 */
export let getActiveRiddles = async (id: number, editor: boolean, expand: number) => {
    const hunt = await loadHunt(id, 'riddles', 'riddles.riddles', ...expandIf(expand > 0, 'riddles.files', 'riddles.hints'));
    const arr: response.Riddle[] = [];
    hunt.riddles.forEach(element => {
        if (allDepsSolved(element) && !element.solved) arr.push(new response.Riddle(element, editor));
    });
    return arr;
};

/**
 * Handle ```/hunt/<id>/graph``` endpoint,
 * scope: editor, receiver.
 *
 * @param id The hunt id
 */
export let getGraph = async (id: number, editor: boolean) => {
    let riddles = await getConnection().getRepository(Riddle).find({ where: { hunt: id }, relations: ['riddles'] });
    if (!editor) riddles = riddles.filter(elem => elem.riddles.reduce((flat: boolean, e) => (flat && e.solved) ? true : false, true));
    return new response.Graph(riddles);
};
