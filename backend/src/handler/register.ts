import { getConnection } from 'typeorm';
import { Register } from '../endpoints/requests';
import { User } from '../entities/User';
import { Error } from '../error';
import { hashPw, validateEmail } from '../utils/utils';

/**
 * Handle ```POST /user/register``` endpoint,
 * scope: unprotected
 *
 * @param data The registration information
 */
export let register = async (data: Register) => {
    data.email = data.email.toLocaleLowerCase();
    if (!validateEmail(data.email))
        throw new Error(400, 'invalid email');
    if (data.password.length < 8 ||
        !data.password.match('.*[0-9].*') ||
        !data.password.match('.*[a-z].*') ||
        !data.password.match('.*[A-Z].*'))
        throw new Error(400, 'password not strong enough');
    const user = new User();
    user.name = data.username;
    user.email = data.email;
    user.password = hashPw(data.password);
    user.joined = new Date();
    user.lastLogin = user.joined;
    try {
        await getConnection().getRepository(User).insert(user);
    } catch (e) {
        throw new Error(400, 'bad request');
    }
};
