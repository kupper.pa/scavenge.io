import { getConnection } from 'typeorm';
import { AddHint, EditHint } from '../endpoints/requests';
import * as response from '../endpoints/responses';
import { Hint } from '../entities/Hint';
import { Error } from '../error';
import { isHintUnlocked, loadRiddle } from '../utils/db';

/**
 * Handle ```GET /riddle/<id>/hint``` endpoint,
 * scope: editor, receiver
 *
 * @param id The riddle id
 */
export let listHints = async (id: number, editor: boolean) => {
    const hints = await getConnection().getRepository(Hint).find({ where: { riddle: id }, relations: editor ? [] : ['riddle', 'riddle.riddles'] });
    return new response.Hints(hints.filter(elem => editor || isHintUnlocked(elem)), editor);
};

/**
 * Handle ```POST /riddle/<id>/hint``` endpoint,
 * scope: editor
 *
 * @param id The riddle id
 * @param data An AddHint instance
 */
export let addHint = async (id: number, data: AddHint) => {
    if (data.displayAt !== undefined && data.displayInHours !== undefined) throw new Error(400, 'display time not clear');
    const riddle = await loadRiddle(id, 'hunt');
    await getConnection().getRepository(Hint).insert({ ...data, riddle });
};

/**
 * Handle ```PUT /hint/<id>/``` endpoint,
 * scope: editor
 *
 * @param id The riddle id
 * @param data An EditHunt instance
 */
export let editHint = async (id: number, data: EditHint) => {
    if (data.displayInHours !== undefined && data.displayInHours !== undefined) throw new Error(400, 'display time not clear');
    const hint = await getConnection().getRepository(Hint).findOne({ id });
    await getConnection().getRepository(Hint).save({ ...hint, ...data });
};

/**
 * Handle ```GET /hint/<id>``` endpoint,
 * scope: editor, receiver
 *
 * @param id The hint id
 */
export let getHint = async (id: number, editor: boolean) => {
    const hint = await getConnection().getRepository(Hint).findOne({ where: { id }, relations: ['riddle', 'riddle.riddles', 'riddle.hunt', 'riddle.hunt.receiver'] });
    if (!editor) {
        if (!isHintUnlocked(hint)) throw new Error(403, 'unauthorized');
        if (!hint.displayed) {
            hint.displayed = new Date();
            await getConnection().getRepository(Hint).save(hint);
        }
    }
    return new response.Hint(hint, editor);
};

/**
 * Handle ```DELETE /hint/<id>``` endpoint,
 * scope: editor
 *
 * @param id The hint id
 */
export let removeHint = async (id: number) => {
    const hint = await getConnection().getRepository(Hint).findOne({ where: { id }, relations: ['riddle', 'riddle.hunt'] });
    await getConnection().getRepository(Hint).delete(hint);
};
