import { Request, Response } from 'express';
import { join } from 'path';
import { get } from 'request';
import { path } from '../config';
import { isDev, isProd } from '../config';
import { Error } from '../error';

/**
 * Handle all frontend endpoints and serve index.html,
 * scope: unprotected
 *
 * @param _ Express request (not used)
 * @param res Express response
 */
export let indexHtml = (_: Request, res: Response) => {
  if (isProd)
    res.sendFile(join(path.staticFiles, 'index.html'));
  else if (isDev)
    get('http://localhost:8000/index.html').pipe(res);
};

/**
 * Handle all requests to /api/* which don't belong to a route to get a json error instead of default html
 */
export let noRoute = () => { throw new Error(404, 'no route'); };
