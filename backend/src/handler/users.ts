import { compare } from 'bcryptjs';
import { getConnection } from 'typeorm';
import { Password, PasswordChange, Search } from '../endpoints/requests';
import * as response from '../endpoints/responses';
import { User } from '../entities/User';
import { Error } from '../error';
import { loadUser } from '../utils/db';
import { hashPw } from '../utils/utils';

/**
 * Handle ```GET /user/me``` enpoint,
 * scope: authenticated
 *
 * @param user The user currently logged in
 * @returns All information about the user currently logged in
 */
export let me = (user: User) => new response.Me(user);

/**
 * Handle ```GET /user/<id>``` endpoint,
 * scope: authenticated
 *
 * @param id The id of a user
 * @returns Public information about a user
 */
export let getUser = async (id: number) => new response.User(await loadUser(id));

/**
 * Handle ```DELETE /user/remove``` endpoint,
 * scope: authenticated
 *
 * @param data The password of the user currently logged in for verfication
 * @param user The user currently logged in
 */
export let deleteUser = async (data: Password, user: User) => {
    if (!await compare(data.password, user.password))
        throw new Error(403, 'invalid password');
    await getConnection().createQueryBuilder().delete().from('user_created__hunt')
        .where('userId = :uid', { uid: user.id }).execute();
    if (!await getConnection().getRepository(User).delete(user))
        throw new Error();
};

/**
 * Handle ```GET /user/search``` endpoint,
 * scope: authenticated
 *
 * @param query A query string for the user name
 */
export let searchUser = async (data: Search) => {
    const users = await getConnection()
        .getRepository(User)
        .createQueryBuilder('user')
        .where('user.name like :query', { query: `%${data.q}%` })
        .getMany();
    return new response.Users(users);
};

/**
 * Handle ```PUT /user/password``` endpoint,
 * scope: authenticated
 *
 * @param user The use currently logged in
 * @param data The old password for verfication and the new password
 */
export let changePassword = async (user: User, data: PasswordChange) => {
    if (!await compare(data.oldPassword, user.password))
        throw new Error(403, 'invalid password');
    user.password = hashPw(data.newPassword);
    await getConnection().getRepository(User).save(user);
};
