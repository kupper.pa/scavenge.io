import { createHash, randomBytes } from 'crypto';
import { Request, Response } from 'express';
import { File, IncomingForm } from 'formidable';
import * as fs from 'fs';
import { getConnection } from 'typeorm';
import * as config from '../config';
import { File as FileEntity } from '../entities/File';
import { Error } from '../error';
import { loadRiddle } from '../utils/db';
import { filePath } from '../utils/utils';

/**
 * Handle ```POST /riddle/<id>/file``` endpoint and store recerived file,
 * scope: editor
 *
 * @param req Express request
 * @param res Express response
 */
export let uploadFile = async (req: Request, resp: Response) => {
    const riddle = await loadRiddle(req.params.id, 'hunt', 'files');
    if (!riddle) throw new Error(404, 'riddle not found');
    if (riddle.files.length >= config.filehosting.maxFilesPerRiddle) throw new Error(403, 'max. number of files reached');
    const form = new IncomingForm();
    form.maxFileSize = config.filehosting.maxFileSize;
    await new Promise((res, rej) => {
        form.on('file', async (_, file: File) => {
            const name = file.name;
            const mimetype = file.type;
            const hash = createHash('sha256').update(randomBytes(16).toString('hex') + file.name).digest('hex');
            fs.copyFileSync(file.path, filePath(hash));
            fs.unlinkSync(file.path);
            await getConnection().getRepository(FileEntity).insert({ name, mimetype, hash, riddle });
        });
        form.parse(req, err => err ? rej(err) : res());
    });
    resp.send();
};

/**
 * Handle ```GET /riddle/:id/file/:hash``` endpoint and serve requested file,
 * scope: editor, receiver
 *
 * @param req Express request
 * @param res Express response
 */
export let getFile = async (req: Request, res: Response) => {
    const data = await getConnection().getRepository(FileEntity).findOne({ hash: req.params.hash });
    res.type(data.mimetype).download(filePath(req.params.hash), data.name);
};

/**
 * Handle ```DELETE /riddle/:id/file/:hash``` endpoint and delete file,
 * scope: editor
 *
 * @param id The riddle id, used for authorization
 * @param hash The hash of the file
 */
export let deleteFile = async (_: number, hash: string) => {
    const file = await getConnection().getRepository(FileEntity).findOne({ hash });
    await getConnection().getRepository(FileEntity).delete(file);
    fs.unlinkSync(filePath(hash));
};
