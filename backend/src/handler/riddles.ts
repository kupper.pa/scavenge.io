import { getConnection } from 'typeorm';
import { AddRiddle, EditRiddle, ID, IDArray, Solution } from '../endpoints/requests';
import * as response from '../endpoints/responses';
import { Predecessors } from '../endpoints/responses';
import { File } from '../entities/File';
import { Riddle } from '../entities/Riddle';
import { Error } from '../error';
import { createGraphFromRiddles, deleteRiddle, loadHunt, loadRiddle, possiblePredecessor, possiblePredecessors, solveRiddleDb } from '../utils/db';

/**
 * Handle ```POST /hunt/<id>/riddle``` endpoint,
 * scope: editor
 *
 * @param id The hunt id
 * @param data An AddRiddle instance
 */
export let addRiddle = async (id: number, data: AddRiddle) => {
    const hunt = await loadHunt(id);
    const riddle = new Riddle();
    riddle.title = data.title;
    riddle.description = data.description;
    riddle.solution = data.solution;
    riddle.hunt = hunt;
    await getConnection().getRepository(Riddle).save(riddle);
    return new response.Riddle(riddle, true);
};

/**
 * Handle ```GET /riddle/<id>``` endpoint,
 * scope: editor and receiver
 *
 * @param id The riddle id
 * @param editor A boolean indicating whether the user is editor or receiver
 */
export let getRiddle = async (id: number, editor: boolean) => {
    const riddle = await loadRiddle(id, 'files', 'hints', 'riddles', 'hunt');
    return new response.Riddle(riddle, editor, new Predecessors(await possiblePredecessors(riddle)));
};

/**
 * Handle ```PUT /riddle/<id>``` endpoint,
 * scope: editor
 *
 * @param id The riddle id
 * @param data
 */
export let editRiddle = async (id: number, data: EditRiddle) => {
    const riddle = await loadRiddle(id, 'hunt');
    await getConnection().getRepository(Riddle).save({ ...riddle, ...data });
};

/**
 * Handle ```DELETE /riddle/<id>``` endpoint,
 * scope: editor
 *
 * @param id The riddle id
 */
export let removeRiddle = async (id: number) => {
    const riddle = await loadRiddle(id, 'hunt', 'files');
    await deleteRiddle(riddle);
};

/**
 * Handle ```GET /riddle/<id>/file``` endpoint,
 * scope: editor and receiver
 *
 * @param id the riddle id
 */
export let listFiles = async (id: number) => {
    const files = await getConnection().getRepository(File).find({ where: { riddle: id } });
    return new response.Files(files);
};

/**
 * Handle ```GET /riddle/<id>/predecessor``` endpoint,
 * scope: editor, receiver
 *
 * @param id the riddle id
 */
export let getPredecessors = async (id: number) => {
    const riddle = await loadRiddle(id, 'riddles');
    return new response.Predecessors(riddle.riddles);
};

/**
 * Handle ```POST /riddle/<id>/predecessor``` endpoint,
 * scope: editor
 *
 * @param id The riddle id
 * @param depId The riddle id of the predecessor that should be added
 */
export let addPredecessor = async (id: number, depId: ID) => {
    const riddle = await loadRiddle(id, 'riddles');
    const dep = await loadRiddle(depId.id, 'riddles');
    const graph = createGraphFromRiddles(await getConnection().getRepository(Riddle).find({ where: { hunt: riddle.hunt.id }, relations: ['riddles'] }));
    if (!await possiblePredecessor(riddle, dep, graph)) throw new Error(400, 'invalid dependencies, maybe there\'s a loop');
    await getConnection().createQueryBuilder().insert().into('riddle_riddles__riddle').values({ riddleId_1: id, riddleId_2: depId }).execute();
};

/**
 * Handle ```DELETE /riddle/<id>/predecessor``` endpoint,
 * scope: editor
 *
 * @param id The riddle id
 * @param depId The riddle id of the predecessor that should be removed
 */
export let removePredecessor = async (id: number, depId: ID) => {
    await loadRiddle(id);
    await loadRiddle(depId.id);
    await getConnection().createQueryBuilder().delete().from('riddle_riddles__riddle').where({ riddleId_1: id, riddleId_2: depId }).execute();
};

/**
 * Handle ```PUT /riddle/<id>/predecessor``` endpoint,
 * scope: editor
 *
 * @param id The riddle id
 * @param data An object containing an array of the predecessor ids which should be set
 */
export let setPredecessors = async (id: number, data: IDArray) => {
    const riddle = await loadRiddle(id, 'riddles', 'hunt');
    const riddles = await Promise.all(data.ids.map(rid => loadRiddle(rid, 'riddles')));
    await getConnection().createQueryBuilder().delete().from('riddle_riddles__riddle').where({ riddleId_1: id }).execute();
    const graph = createGraphFromRiddles(await getConnection().getRepository(Riddle).find({ where: { hunt: riddle.hunt.id }, relations: ['riddles'] }));
    const validRiddles = await Promise.all(riddles.map(dep => possiblePredecessor(riddle, dep, graph)));
    for (const elem of validRiddles) if (!elem) throw new Error(400, 'invalid dependencies, maybe there\'s a loop');
    await Promise.all(data.ids.map(depId =>
        getConnection().createQueryBuilder().insert().into('riddle_riddles__riddle').values({ riddleId_1: id, riddleId_2: depId }).execute()
    ));
};

/**
 * Handle ```POST /riddle/<id>/solve``` endpoint,
 * scope: editor, receiver.
 *
 * Try to match the solution entered by the user via regular expressions and solve the riddle if it's correct.
 * If all riddles of the hunt are completed, the hunt is also soved.
 *
 * @param id The riddle id
 */
export let solveRiddle = async (id: number, { solution }: Solution) => {
    const riddle = await loadRiddle(id, 'hunt', 'hunt.riddles');
    if (solution === riddle.solution
        || ((riddle.solution.startsWith('^') && riddle.solution.endsWith('$') && solution.match(riddle.solution))))
        await solveRiddleDb(riddle);
    else throw new Error(400, 'wrong answer');
};

/**
 * Handle ```GET /riddle/<id>/possiblepredecessors``` endpoint,
 * scope: editor
 *
 * @param id The riddle id
 */
export let getPossiblePredecessors = async (id: number) => {
    const riddle = await loadRiddle(id, 'hunt', 'riddles');
    return new response.Predecessors(await possiblePredecessors(riddle));
};
