import { Request, Response } from 'express';
import { Request as Oauth2Request, Response as Oauth2Response } from 'oauth2-server';
import { RedisClient } from 'redis';
import { RevokeToken } from '../endpoints/requests';
import { Error } from '../error';
import { delAsync } from '../utils/redis';

/**
 * Handle ```POST /user/logout``` endpoint,
 * scope: authenticated
 *
 * @param tokens Access and/or refresh token that will be deleted
 * @param redis The redis client instance where tokens are stored
 */
export let revokeToken = async (tokens: RevokeToken, redis: RedisClient) => (
    tokens.accessToken && await delAsync(redis, tokens.accessToken),
    tokens.refreshToken && await delAsync(redis, tokens.refreshToken)
);

/**
 * Handle ```* /user/login``` enpoint,
 * scope: unprotected
 *
 * @param req Express request
 * @param res Express response
 */
export let obtainToken = async (req: Request, res: Response) => {
    try {
        const token = await req.oauth.token(new Oauth2Request(req), new Oauth2Response(res));
        token.clientId = token.client.id;
        delete token.client;
        token.userId = token.user.id;
        delete token.user;
        res.json(token);
    } catch (e) {
        throw new Error(e.code || 500, e.message);
    }
};
