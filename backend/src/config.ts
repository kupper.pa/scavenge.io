import { Client as Oauth2Config } from 'oauth2-server';
import { join } from 'path';
import { ClientOpts as RedisConfig } from 'redis';
import { ConnectionOptions as TypeormConfig } from 'typeorm';
import * as devConfig from './config/develop.config.json';
import * as prodConfig from './config/production.config.json';

export let insertTestData = Boolean(process.env.TEST_DATA);
export let isProd = process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'prod';
export let isDev = process.env.NODE_ENV === 'develop' || process.env.NODE_ENV === 'dev';
if (!isProd && !isDev) throw 'The NODE_ENV must be set to either \'develop\' or \'production\'';

const config = (isProd ? prodConfig : devConfig) as any;

class Path {
    constructor(public volumeMount: string, public staticFiles: string) { }
}

class FileHosting {
    maxFileSize: number;
    maxFilesPerRiddle: number;
}

export let oauth: Oauth2Config = config.oauth;
export let typeorm: TypeormConfig = { ...config.typeorm, entities: config.typeorm.entities.map((e: string) => join(__dirname, e)) };
export let redis: RedisConfig = config.redis;
export let path: Path = new Path(config.path.volumeMount, join(__dirname, config.path.staticFiles));
export let delay = config.delay;
export let filehosting: FileHosting = { maxFileSize: 1024 * 1024 * 1024, maxFilesPerRiddle: 10, ...config.filehosting };

if (config.display) console.log(JSON.stringify({ oauth, typeorm, redis, paths: path, filehosting, delay }, null, 2));
