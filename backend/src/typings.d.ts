import { User } from './entities/User';
import * as OAuth2Server from "oauth2-server";
import { RedisClient } from "redis";

declare global {
   namespace Express {
      export interface Request {
         user: User;
         editor?: boolean;
         oauth: OAuth2Server;
         redis: RedisClient;
      }
   }
}