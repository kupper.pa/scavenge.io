import { Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Hunt } from './Hunt';

@Entity('User')
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'char',
        length: 31,
        unique: true
    })
    name: string;

    @Column({
        type: 'char',
        length: 255,
        unique: true
    })
    email: string;

    @Column({
        type: 'char',
        length: 255
    })
    password: string;

    @Column()
    joined: Date;

    @Column()
    lastLogin: Date;

    @OneToMany(_ => Hunt, hunt => hunt.receiver)
    hunts: Hunt[];

    @ManyToMany(_ => Hunt, hunt => hunt.createdBy)
    @JoinTable()
    created: Hunt[];

}
