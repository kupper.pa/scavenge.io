import { Column, Entity, ManyToOne, PrimaryColumn } from 'typeorm';
import { Riddle } from './Riddle';

@Entity('File')
export class File {

    @PrimaryColumn({
        type: 'char',
        length: 64
    })
    hash: string;

    @Column({
        type: 'char',
        length: 255
    })
    name: string;

    @Column({
        type: 'char',
        length: 64
    })
    mimetype: string;

    @ManyToOne(_ => Riddle, riddle => riddle.files)
    riddle: Riddle;

}
