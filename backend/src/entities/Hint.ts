import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Riddle } from './Riddle';

@Entity('Hint')
export class Hint {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'char',
        length: 255
    })
    title: string;

    @Column()
    description: string;

    @Column({
        nullable: true
    })
    displayAt: Date;

    @Column({
        type: 'int',
        nullable: true
    })
    displayInHours: number;

    @Column({
        nullable: true
    })
    displayed: Date;

    @ManyToOne(_ => Riddle, riddle => riddle.hints)
    riddle: Riddle;

}
