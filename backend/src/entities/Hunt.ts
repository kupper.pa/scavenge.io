import { Column, Entity, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Riddle } from './Riddle';
import { User } from './User';

@Entity('Hunt')
export class Hunt {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'char',
        length: 255
    })
    title: string;

    @Column({
        type: 'text'
    })
    description: string;

    @Column({
        nullable: true
    })
    start: Date;

    @Column({
        nullable: true
    })
    completed: Date;

    @Column({
        type: 'text'
    })
    reward: string;

    @ManyToOne(_ => User, user => user.hunts)
    receiver: User;

    @ManyToMany(_ => User, user => user.created)
    createdBy: User[];

    @OneToMany(_ => Riddle, riddle => riddle.hunt)
    riddles: Riddle[];

}
