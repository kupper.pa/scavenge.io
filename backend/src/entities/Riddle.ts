import { Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { File } from './File';
import { Hint } from './Hint';
import { Hunt } from './Hunt';

@Entity('Riddle')
export class Riddle {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'char',
        length: 255
    })
    title: string;

    @Column({
        type: 'text',
        nullable: true
    })
    description: string;

    @Column({
        nullable: true
    })
    solved: Date;

    @Column({
        type: 'char',
        length: 255,
        nullable: true
    })
    solution: string;

    @ManyToOne(_ => Hunt, user => user.riddles)
    hunt: Hunt;

    @ManyToMany(_ => Riddle)
    @JoinTable()
    riddles: Riddle[];

    @OneToMany(_ => File, data => data.riddle)
    files: File[];

    @OneToMany(_ => Hint, hint => hint.riddle)
    hints: Hint[];

}
