import { Request } from 'express';
import { Error } from '../error';

const requiredKey = Symbol('required');
const optionalKey = Symbol('optional');
const notEmptyKey = Symbol('not_empty');
const sourceTypeKey = Symbol('source_type');

/**
 * Constructible is a typesave generic constructor function
 */
export type Constructible<T extends Object = Object> = new (...args: any[]) => T;

/**
 * Defines (if not already present) an empty array in the metadata on the target object
 * Otherwise it appends the given property name and its type as a tuple to the previously defined array
 */
const store = <K>(target: Object, key: K, propertyName: string) => {
    if (!Reflect.hasMetadata(key, target)) Reflect.defineMetadata(key, [], target); // toLowerCase is required because the value is the constructor name of the type
    Reflect.getMetadata(key, target).push([propertyName, Reflect.getMetadata('design:type', target, propertyName).name.toLowerCase()]);
};

const getStore = <T>(key: T, target: Object): Array<[string, string]> => Reflect.getMetadata(key, target);

/**
 * A generic wrapper around a constructor function
 * @param fn A function that can be used to modify the new instance e.g. set metadata
 * @returns An class decorator
 */
const wrap = (fn: (instance: any) => any) => {
    return <T extends new (...constructorArgs: any[]) => any>(constructor: T) => {
        const newConstructor: any = function(...args: any[]) {
            const func: any = function() { return new constructor(...args); };
            func.prototype = constructor.prototype;
            const instance: any = new func();
            fn(instance);
            return instance;
        };
        newConstructor.prototype = constructor.prototype;
        return newConstructor;
    };
};

const multi = (source: symbol) =>
    wrap(instance => {
        if (!getStore(optionalKey, instance) && !getStore(requiredKey, instance))
            throw TypeError(`class '${instance.constructor.name}' should have min one required or optional fields`);
        Reflect.defineMetadata(sourceTypeKey, source, instance);
    });

const single = (source: symbol) =>
    wrap(instance => {
        const name = instance.constructor.name;
        const reqStore = getStore(requiredKey, instance);
        const optStore = getStore(optionalKey, instance);
        if (reqStore && optStore)
            throw TypeError(`class '${name}' sould not have either one optional or required field but not both`);
        if (reqStore && !optStore && reqStore.length !== 1)
            throw TypeError(`class '${name}' sould only have one required field`);
        if (optStore && !reqStore && optStore.length !== 1)
            throw TypeError(`class '${name}' sould only have one optional field`);
        Reflect.defineMetadata(sourceTypeKey, source, instance);
    });

const PARAMS = Symbol('params');
const PARAM = Symbol('param');
const QUERY = Symbol('query');
const QUERIES = Symbol('queries');
const BODY = Symbol('body');
const REQUEST = Symbol('request');

export let notEmpty = wrap(instance => Reflect.defineMetadata(notEmptyKey, true, instance));
export let reqBody = multi(BODY);
export let reqRaw = single(REQUEST);
export let reqQueries = multi(QUERIES);
export let reqQuery = single(QUERY);
export let reqParam = single(PARAM);
export let reqParams = multi(PARAMS);

/**
 * Decorator for required attributes
 */
export let required = (target: Object, propertyName: string) =>
    store(target, requiredKey, propertyName);

/**
 * Decorator for required attributes
 */
export let optional = (target: Object, propertyName: string) =>
    store(target, optionalKey, propertyName);

/**
 * Populate an Instance form a express request
 */
export let populate = <T>(instance: T, req: Request): any => {
    const sourceType = Reflect.getMetadata(sourceTypeKey, instance);
    switch (sourceType) {
        case BODY:
            populateFromBody(instance, req);
            break;
        case QUERIES:
            populateFromQueries(instance, req);
            break;
        case PARAMS:
            populateFromParams(instance, req);
            break;
        case PARAM:
            instance = populateFromParam(instance, req);
            break;
        case QUERY:
            instance = populateFromQuery(instance, req);
            break;
        case REQUEST:
            instance = populateFromReq(instance, req);
            break;
        default:
            throw TypeError(`unknown instance '${instance}' with source type '${sourceType}'`);
    }
    return instance;
};

/**
 * Checks if a class can be completly empty
 */
const notEmptyCheck = (target: Object, source: string) => {
    if (Reflect.getMetadata(notEmptyKey, target) && !Object.entries(target).length) throw new Error(400, `request ${source} should not be empty`);
};

/**
 * Checks if a given value is undefined
 * This helper function is needed because thanks to js u cannot write the undefined check in a if condition with other bools
 */
const isUndefined = (target: any) => target === undefined;

/**
 * Checks if the value is undefined and throws if so
 */
const requiredFn = (value: any, fieldName: string): any => { if (isUndefined(value)) throw new Error(400, `missing parameter '${fieldName}'`); };

const parseLoop = <T extends Object>(instance: T, from: any, key: any, parse: (value: any, fieldName: string, expType: string) => any = value => value) => {
    const skip = (key === requiredKey) ? requiredFn : isUndefined;
    (getStore(key, instance) || []).forEach(([fieldName, expType]) =>
        skip(from[fieldName], fieldName) || (instance[fieldName] = parse(from[fieldName], fieldName, expType)));
};

const parseLoopTypeCheck = <T extends Object>(instance: T, from: any, key: any) =>
    parseLoop(instance, from, key, (value: string, fieldName: string, expType: string) => {
        const typeOfValue = typeof value;
        if (expType === 'date' && typeOfValue === 'string') return new Date(value);
        if (expType === 'array' && typeOfValue === 'object' && Array.isArray(value)) return value;
        if (typeOfValue !== expType) throw new Error(400, `invalid field '${fieldName}', excepted type '${expType}' instead of '${typeOfValue}'`);
        return value;
    });

const parseMulti = <T extends Object>(instance: T, source: string, from: any, fn: (instance: any, from: any, key: any) => void) =>
    (fn(instance, from, requiredKey), fn(instance, from, optionalKey), notEmptyCheck(instance, source));

const parseSingle = <T extends Object>(instance: T, source: string, from: any): any => {
    const reqStore = getStore(requiredKey, instance);
    const propName = (reqStore || getStore(optionalKey, instance))[0][0];
    const value = from[propName];
    if (reqStore && isUndefined(value)) throw new Error(400, `missing field '${propName}' in '${source}'`);
    return value;
};

const populateFromBody = <T extends Object>(instance: T, { body }: Request): void => parseMulti(instance, 'body', body, parseLoopTypeCheck);
const populateFromQueries = <T extends Object>(instance: T, { query }: Request): void => parseMulti(instance, 'query', query, parseLoop);
const populateFromParams = <T extends Object>(instance: T, { params }: Request): void => parseMulti(instance, 'parameter', params, parseLoop);

const populateFromQuery = <T extends Object>(instance: T, { query }: Request): any => parseSingle(instance, 'query', query);
const populateFromParam = <T extends Object>(instance: T, { params }: Request): any => parseSingle(instance, 'router parameters', params);
const populateFromReq = <T extends Object>(instance: T, req: Request): any => parseSingle(instance, 'express request', req);
