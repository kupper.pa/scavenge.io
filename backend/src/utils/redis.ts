import { RedisClient } from 'redis';
import { StoreToken } from '../oauth/types';

/**
 * Loads user information associated to the given access token from the redis server asynchronous
 *
 * @param redis The redis client instance
 * @param accessToken The access token that sould be looked up
 * @returns A parsed JSON Object of the associated information or null if the access token could not be found
 */
export let getTokenAsync = async (redis: RedisClient, accessToken: string): Promise<StoreToken> =>
    new Promise((res, rej) =>
        redis.get(accessToken, (err, rep) => err ? rej(err) : res(rep ? JSON.parse(rep) : null)));

/**
 * Deletes a key value pair from the redis instance asynchronous
 *
 * @param redis The redis client instance
 * @param key The key that should be deleted
 */
export let delAsync = (redis: RedisClient, key: string): Promise<void> =>
    new Promise((res, rej) =>
        redis.del(key, err => err ? rej(err) : res()));
