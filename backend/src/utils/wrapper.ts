import { NextFunction, Request, Response } from 'express';
import { ID } from '../endpoints/requests';
import { InternalError } from '../error';
import { populate } from './meta';
import { Constructible } from './meta';

/**
 * RawHandler is a handler function that gets a express Request and response
 * This handler has to validate the request data on its own
 */
export type RawHandler = (req: Request, res: Response) => any;

/**
 * Middleware that takes only the request and returns nothing
 * This forces consistency in error handling and response management
 */
export type Middleware = (req: Request) => void;

/**
 * Handler that uses request validation instead of raw express request
 * The return value will be json encoded and send as response
 */
export type Handler = (...data: any[]) => any;

/**
 * Wraps a RawHandler
 * Catches all errors and propagates them to the error handler middleware
 *
 * @param path The route/path that should be handled by the handler
 * @param handler The RawHandler that handles the given route
 */
export let wrapRawRoute = (path: string, handler: RawHandler) =>
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      await handler(req, res);
    } catch (e) {
      next(new InternalError(path, e));
    }
  };

/**
 * Wraps a Handler
 * Catches all errors and propagates them to the error handler middleware
 *
 * @param path The route/path that should be handled by the handler
 * @param handler The Handler that handles the given route
 * @param ccs Constructor functions of the data that the handler takes
 */
export let wrapRoute = (path: string, handler: Handler, ...ccs: Constructible[]) => {
  if (path.includes(':id') && !ccs.includes(ID)) ccs.unshift(ID);
  if (ccs.length !== handler.length)
    throw TypeError(`function argument count '${handler.length}' from route '${path}' does not match provided arguments of '${ccs.length}'`);
  return wrapRawRoute(path, async (req: Request, res: Response) => {
    const result = await handler(...ccs.map(cc => populate(new cc(), req)));
    result ? res.json(result) : res.send();
  });
};

/**
 * Wraps Middleware
 * Catches all errors and propagates them to the error handler middleware
 *
 * @param path The route/path that should be handled by the handler
 * @param handler The Handler that handles the given route
 * @param ccs Constructor functions of the data that the handler takes
 */
export let wrapMiddelware = (source: string, fn: Middleware) =>
  async (req: Request, _: Response, next: NextFunction) => {
    try {
      await fn(req);
      next();
    } catch (e) {
      next(new InternalError(source, e));
    }
  };
