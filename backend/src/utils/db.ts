import { promises as fs } from 'fs';
import { getConnection } from 'typeorm';
import { Connection, ConnectionOptions, createConnection } from 'typeorm';
import { Edge, Graph } from '../datastructures/graph';
import { Hint } from '../entities/Hint';
import { Hunt } from '../entities/Hunt';
import { Riddle } from '../entities/Riddle';
import { User } from '../entities/User';
import { Error } from '../error';
import { sleep, validateEmail } from '../utils/utils';
import { filePath } from './utils';

/**
 * Throws an error if no entities could be loaded.
 * Used to prevent starting the app with misconfigured orm config
 *
 * @param conn Active Typeorm connection
 */
const entitysCheck = (conn: Connection) => {
    if (conn.entityMetadatas.length) return conn;
    throw 'no entitys found';
};

/**
 * Tries to connect to a database
 *
 * @param config Typeorm config object
 * @param delay This delay will only apply if the connection could not be established
 */
const tryConnect = async (config: ConnectionOptions, delay: number) => {
    let conn: Connection;
    try {
        conn = await createConnection(config);
    } catch (e) {
        console.error(`typeorm: ${e.message}`);
        return await sleep(delay);
    }
    return entitysCheck(conn);
};

/**
 * Tries to create a database connection with the given options
 *
 * @param config Typeorm config object
 * @param delay This delay will only apply if the connection could not be established
 * @param retries How many times it will be tried to connect to the database
 */
export let connect = async (config: ConnectionOptions, delay: number = 1000, retries: number = 100) => {
    while (retries > 0) {
        if (await tryConnect(config, delay)) return;
        retries--;
    }
    throw 'could not connect to database configured in typeorm';
};

/**
 * Loads a user by its id from the database and throws an error on failure
 *
 * @param id The id of the user
 * @param err An error that is thrown on failure. If not set the default error ```404: user not found``` is thrown.
 */
export let loadUser = async (id: number, err?: Error) => {
    const user = await getConnection().getRepository(User).findOne({ id });
    if (user) return user;
    throw err || new Error(404, 'user not found');
};

/**
 * Loads a user by its name from the database and throws an error on failure
 *
 * @param name The name of the user
 * @param err An error that is thrown on failure. If not set the default error ```404: user not found``` is thrown.
 */
export let loadUserByNameOrEmail = async (name: string, err?: Error) => {
    const user = validateEmail(name)
        ? await getConnection().getRepository(User).findOne({ email: name })
        : await getConnection().getRepository(User).findOne({ name });
    if (user) return user;
    throw err || new Error(404, 'user not found');
};

/**
 * Loads a hunt by its id from the database and throws an error on failure
 *
 * @param id The id of the hunt
 * @param err An error that is thrown on failure. If not set the default error ```404: hunt not found``` is thrown.
 */
export let loadHunt = async (id: number, ...relations: string[]) => {
    relations.push('receiver');
    const hunt = await getConnection().getRepository(Hunt).findOne({ where: { id }, relations });
    if (hunt) return hunt;
    throw new Error(404, 'hunt not found');
};

/**
 * Loads a riddle by its id from the database and throws an error on failure
 *
 * @param id The id of the riddle
 * @param relations The relations that should be joined
 */
export let loadRiddle = async (id: number, ...relations: string[]) => {
    const riddle = await getConnection().getRepository(Riddle).findOne({ where: { id }, relations });
    if (riddle) return riddle;
    throw new Error(404, 'riddle not found');
};

/**
 * Delete a hunt from the database including all riddles, files and hints belonging to it
 *
 * @param hunt The hunt that should be deleted
 */
export let deleteHunt = async (hunt: Hunt) => {
    await Promise.all((hunt.riddles || []).map(deleteRiddle));
    await getConnection().getRepository(Hunt).delete(hunt);
};

/**
 * Delete a riddle from the database including all files and hints belonging to it
 *
 * @param riddle The riddle that should be deleted
 */
export let deleteRiddle = async (riddle: Riddle) => {
    const files = riddle.files;
    riddle.files = [];
    riddle.hints = [];
    await getConnection().getRepository(Riddle).save(riddle);
    await getConnection().getRepository(Riddle).delete(riddle);
    await Promise.all(files.map(element => { try { fs.unlink(filePath(element.hash)); } catch (e) { console.log(e); } }));
};

/**
 * Update all necessary fields in the database when a riddle was solved
 *
 * @param riddle The riddle that was solved
 */
export let solveRiddleDb = async (riddle: Riddle) => {
    riddle.solved = new Date();
    if (await allRiddlesSolved(riddle.hunt.riddles.filter(elem => elem.id !== riddle.id)))
        riddle.hunt.completed = riddle.solved;
    await getConnection().getRepository(Riddle).save(riddle);
    await getConnection().getRepository(Hunt).save(riddle.hunt);
};

/**
 * Check if all previous riddles are solved
 *
 * @param riddle The riddle with joined riddles
 * @returns True iff the riddle is unlocked.
 */
export let allDepsSolved = (riddle: Riddle): boolean => {
    for (const rid of riddle.riddles) if (!rid.solved) return false;
    return true;
};

/**
 * Checks when the riddle was unlocked
 *
 * @param riddle The riddle with joined riddles
 * @returns The date when the riddle was unlocked
 */
export let getUnlockTime = (riddle: Riddle): Date => {
    let date;
    for (const rid of riddle.riddles) {
        const sovledDate = new Date(riddle.solved);
        if (!rid.solved) return null;
        else if (sovledDate > date) date = sovledDate;
    }
    return date;
};

/**
 * Checks whether a hint is unlocked, either by explicit unlock time or after an amount of hours after unlocking the riddle
 *
 * @param hint The hint entity
 */
export let isHintUnlocked = (hint: Hint) => {
    if (hint.displayAt)
        return hint.displayAt <= new Date();
    else if (hint.displayInHours) {
        const unlockTime = getUnlockTime(hint.riddle);
        if (!unlockTime) return false;
        unlockTime.setUTCHours(unlockTime.getUTCHours() + hint.displayInHours);
        return new Date() >= unlockTime;
    }
    return false;
};

/**
 * Checks whether all riddles are solved
 *
 * @param riddles Array of riddles to be checked
 */
export let allRiddlesSolved = async (riddles: Riddle[]) => {
    for (const riddle of riddles)
        if (!riddle.solved) return false;
    return true;
};

/**
 * Checks if a riddle can be a dependency (not already dependency and no circle)
 *
 * @param riddle The riddle
 * @param dependency The dependency that should be checked
 * @param graph A graph containing all riddles of the hunt as vertices and predecessors as directed edges
 */
export let possiblePredecessor = (riddle: Riddle, dependency: Riddle, graph: Graph): Riddle => {
    if (riddle.id === dependency.id) return null;
    for (const r of riddle.riddles)
        if (r.id === dependency.id) return dependency;
    // if (await isDependent(riddle, dependency)) return null;
    if (graph.isPath(dependency.id, riddle.id)) return null;
    return dependency;
};

/**
 * Get all riddles that can be predecessors without creating a loop
 *
 * @param riddle The riddle entity
 * @param riddles All riddles from the hunt
 * @returns All riddles that can be predecessors
 */
export let possiblePredecessors = async (riddle: Riddle) => {
    const riddles = await getConnection().getRepository(Riddle).find({ where: { hunt: riddle.hunt.id }, relations: ['riddles'] });
    const graph = await createGraphFromRiddles(riddles);
    return riddles.map(dep => possiblePredecessor(riddle, dep, graph)).filter(elem => elem);
};

/**
 * Creates a directed graph from riddles with edges to predecessors
 *
 * @param riddles All riddles of a hunt with joined predecessors ('riddles')
 * @returns A graph with all riddles and predecessor relations from a hunt
 */
export let createGraphFromRiddles = (riddles: Riddle[]) => {
    const edges = new Array<Edge>();
    for (const r of riddles)
        for (const r2 of r.riddles)
            edges.push([r.id, r2.id]);
    return new Graph(riddles.map(elem => elem.id), edges);
};
