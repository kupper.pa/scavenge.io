import { hashSync } from 'bcryptjs';
import { join } from 'path';
import { path } from '../config';

/**
 * Creates an array from multiple function parameters
 *
 * @param params Any number of parameters
 */
export let bundle = <T>(...params: T[]) => params;

/**
 * Creates a hash for a given password
 *
 * @param pw The password to be hashed
 */
export let hashPw = (pw: string) => hashSync(pw, 8);

/**
 * Checks whether an email has a valid format
 *
 * @param email The email to be checked
 */
export let validateEmail = (email: string) =>
    /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/.test(email);

/**
 * Sleeps for a given time
 *
 * @param ms Time to sleep in ms
 */
export let sleep = (ms: number): Promise<void> => new Promise(res => setTimeout(res, ms));

/**
 * Creates an absolute path for a given file name
 *
 * @param fileName Name of the file
 */
export let filePath = (fileName: string) => join(path.volumeMount, fileName);
