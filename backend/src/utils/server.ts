import * as express from 'express';
import { Express, Response } from 'express';
import { RequestHandler as Middleware } from 'express-serve-static-core';
import { Constructible } from './meta';
import { Handler, RawHandler, wrapRawRoute, wrapRoute } from './wrapper';

/**
 * All http methodes that express supports
 */
type Method = 'get' | 'post' | 'put' | 'delete' | 'all';
/**
 * This type can either be a Route, a Scope, a SelcetHandler or a SetData instance
 */
type ScopeOrRouteOrSelectOrData = Route | Scope | SelectHandler | SetData;
/**
 * This type can either be a Route or a Scope instance
 */
type ScopeOrRoute = Route | Scope;

/**
 * Represents a single route that will be handled
 */
class Route {
  constructor(private m: Method, private path: string, private handler: Middleware) { }
}

/**
 * Represents a new scope
 * A Scope is used to scope the use of middleware e.g. for auth purposes and role management
 */
class Scope {
  constructor(private mw: Middleware[], private scopeRoutes: ScopeOrRoute[]) { }
}

/**
 * Represents a new Route that can either be a raw route (express) or a typesafe one
 * Raw express routes should not be used unless absolutly needed
 */
class SelectHandler {
  constructor(private m: Method, private path: string) { }
  /**
   * Set the function that handles the given path to @param handler
   */
  handler = (handler: Handler) => new SetData(this.m, this.path, handler);
  /**
   * Set the function that handles the given path to @param handler
   */
  rawHandler = (handler: RawHandler) => new Route(this.m, this.path, wrapRawRoute(this.path, handler));
}

/**
 * SetData is used to potentially add data that should be handled
 */
class SetData {
  constructor(private m: Method, private path: string, private handler: Handler) { }
  /**
   * used to set the request data, takes an arry of constructor functions
   */
  data = (...ccs: Constructible[]) => new Route(this.m, this.path, wrapRoute(this.path, this.handler, ...ccs));
}

/**
 * normalize finalizes the SetData and SelectHandler classes => transfroms them to Routes that can be handled be express
 *
 * @param scopeRoutes Contains unfinalized Routes
 */
const normalize = (scopeRoutes: ScopeOrRouteOrSelectOrData[]): ScopeOrRoute[] =>
  scopeRoutes.map(e => (
    e instanceof SetData && new Route(e['m'], e['path'], wrapRoute(e['path'], e['handler'])) ||
    e instanceof SelectHandler && new Route(e['m'], e['path'], wrapRawRoute(e['path'], (_, res: Response) =>
      res.status(501).send())) || e as ScopeOrRoute));

/**
 * Creates a new Express app
 * @param middlewares some middlewares that will be used by the server e.g. body parsing stuff
 * @param scope  If needed there can be created scopes for middlewares
 */
export let builder = (middlewares: Middleware[], ...scopeRoutes: ScopeOrRouteOrSelectOrData[]): Express => {
  const app = express().use(middlewares);
  const recMount = (s: Scope, mw: Middleware[]) => {
    const mws = [...mw, ...s['mw']];
    s['scopeRoutes'].forEach(e => (
      e instanceof Route && app[e['m']](e['path'], mws, e['handler']) ||
      e instanceof Scope && recMount(e, mws)
    ));
  };
  normalize(scopeRoutes).forEach(e => (
    e instanceof Route && app[e['m']](e['path'], e['handler']) ||
    e instanceof Scope && recMount(e, [])
  ));
  return app;
};

/**
 * Creates a new Scope out of min one middleware and as many as provided Routes and other Scopes
 *
 * @param mw Middlewares that should be used by the Routes in this scode
 * @param scopeRoutes Routes that are in this scope and also some other scopes
 */
export let scope = (mw: Middleware[], ...scopeRoutes: ScopeOrRouteOrSelectOrData[]) =>
  new Scope(mw, normalize(scopeRoutes));

/**
 *  @returns a new route that matches the given path with the http get method
 */
export let get = (path: string) => new SelectHandler('get', path);
/**
 *  @returns a new route that matches the given path with the http post method
 */
export let post = (path: string) => new SelectHandler('post', path);
/**
 *  @returns a new route that matches the given path with the http put method
 */
export let put = (path: string) => new SelectHandler('put', path);
/**
 *  @returns a new route that matches the given path with the http delete method
 */
export let del = (path: string) => new SelectHandler('delete', path);
/**
 *  @returns a new route that matches the given path with the matches all http methodes
 */
export let all = (path: string) => new SelectHandler('all', path);
